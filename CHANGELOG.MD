## 1.7.10
### 1st of December, 2023

1. Updated transistor to currently available in both the standard and HP BOMs.

2. Add option to emboss stimulus in video during video conversion (#97).

3. Fix broken 640x480 undistort (See #95).

4. Addressed #96: Video recording and full frame image recording time
   dependent stimulus is now based on time (not index as was before 1.7.0).

5. Renamed stimulus file for video and full frame recording from 
   'data.csv' to 'stimulation_presented.csv' and 'stimulation_file_used.csv' (#40). 
   Also updated docs to reflect those changes. 

6. Fixed additional typos in docs and updated .readthedocs.yaml.  

## 1.7.9
### 11th of July, 2023

1. Fixed #71 (fix typos and update of docs)

2. Update list of labs using PiVR on website

3. Add 'PiVR used for teaching' on website

## 1.7.8
### 26th of March, 2023

1. Fix bug introduced in 1.7.6. control_file in line 141,
470, 589 & 664 raspberry->RASPBERRY

2. Fix bug introduced in 1.7.6. pre_experiment in line 
183, 206, 364, 424, 484, 529, 888, 1447, 2123, 2255,
2442, 2940 raspberry->RASPBERRY

3. Fix bug introduced in 1.7.6. record_videos_and_image_sequences.py 
in line 507  

## 1.7.7
### 26th of March, 2023

1. Typo (wrong indendation on website)

## 1.7.6
### 26th of March, 2023

1. Fixed #87: PiVR software doesn't work on MacOS.

2. Typos in visualize_tracked_experiment 

3. Wrote documentation for #88. Instead of cloning the PiVR repository
   and installing all packages we provide an OS image which can be used 
   by the user. 

4. Added missing info to Changelog.md from previous 2 commits

## 1.7.5
### 12th of November, 2022

1. updated missing information in doc about files that are saved

## 1.7.4
### 1st of November, 2022

1. Updated installation guide to solve problem described in #78 and 
   fixed a few typos.

## 1.7.3
### 20th of February, 2022

1. Due to the change of having NaN in data.csv I introduced a bug that 
   made using visualize tracked experiment impossible. Fixed it here.

## 1.7.2
### 20th of February, 2022

1. Fixed incorrect path pointer to undistort files in image_data_handling.py

2. In Display Tracked Experiment added option to interpolate head and 
   tail.

3. In Display Tracked Experiment added option to cut trajectory.

4. Display Tracked experiment doesn't throw error if no heads.npy or 
   tails.npy files is present.

5. Fixed bug that would result in the bounding box info not being saved 
   in a time-dependent stimulus experiment (#80).

6. The output data.csv file now contains 'NaN' instead of zero for 
   missing information (#74).

## 1.7.1
### 29th of November, 2021

1. Lots of work on the pivr.org website

## 1.7.0
### 16th of October, 2021

1. Optimized undistort video by using rectifymap and remap.

2. Option to do undistort centroid, head, tail and midpoint during 
   online tracking.

3. Option to select standard or own mtx/dst file for undistort.

4. Found bug in midpoint assignment: The skeleton_x and skeleton_y array 
   was sometimes not sorted. Fixed by using np.sort()

5. Completely changed how PiVR is installed on the Raspberry Pi: The new
   installation uses pip and also installs cv2 which is necessary for 
   online-undistort.

6. Option for user to easily add own undistort calibration files.

7. Added info about bad batches of camera cables leading to confusing 
   errors to FAQ.

8. Title reflects whether cv2 is installed or not for easy 
   troubleshooting.

9. Changed frame based time dependent stimulus to time dependent 
   stimulus.

10. Added FAQ for camera interference (#68).

11. Changed python version during software installation to address #60.

12. Fixed two minor bugs: (1) After running VR experiment it wasn't possible to
    to run a tracking experiment. (2) If user pressed 'Cam On' twice a second camera
    overlay would be created if VR arena was chosen.


## 1.6.11
### 10th of October, 2021

1. Modified PCB for high powered PiVR has been uploaded.

## 1.6.10
### 27th of September, 2021

1. POTENTIAL TEMPORARY fix for #67 (HP PCB not working as expected). 
   Double check that v2.4 really is broken and whether v2.2. works as expected.  

## 1.6.9
### 23th of September, 2021

1. Added option to not save binary and skeleton npy files.

2. Saving the used threshold in data.csv for on-the-fly binary image reconstruction.
 
3. Bounding Box is now saved in data.csv .

4. Added FAQ question based on #57 where camera cable was faulty.

## 1.6.8
### 21th of September, 2021

1. Fixed bug introduced in v1.6.7 which made it impossible to
   simulate online tracking.

## 1.6.7
### 19th of September, 2021

1. Critical fix for Issue #66: Post-hoc tracking will now keep time 
   between recording and tracking intact. 

## 1.6.6
### 15th of September, 2021

1. Merged/remade all 3D models and printed with no PVA support.

2. Make sure Kitspace can display the Readme.md as expected by using 
   absolute links from gitlab instead of relative links.

## 1.6.5
### 7th of September, 2021

1. Enrich Readme to provide a more attractive landing site on both 
   gitlab and Kitspace.org.

## 1.6.4
### 6th of September, 2021

1. Bugfix: Time was wrong when performing post hoc tracking. Fixed.

2. When _undistorted is found in video file name, use that video for 
   post-hoc tracking.

## 1.6.3
### 6th of September, 2021

1. Fix issue #5: Allow PiVR update on Raspberry even after running 
   experiments.

## 1.6.2
### 6th of September, 2021

1. Testing whether Kitspace.org can read the data structure.

## 1.6.1
### 6th of September, 2021

1. For Readthedocs the git calls had to be wrapped in try/except statement.

## 1.6.0
### 6th of September, 2021

1. Fixed Bug in HP version where the stimulus was inverted every time.
   Also inverted output in data.csv back to input (e.g. input is 100,
   need to present 0 to get full light, save 100 to be consistent 
   with standard PiVR version.)

2. After collecting video and performing post-hoc single animal 
   tracking there was a bug when trying to run distance to source 
   analysis due to the two data.csv files. Fixed.

3. When more than one video in folder to analyze, will now directly 
   ask in folder of question which video to choose. 

4. MultiAnimal tracking now produces a larger figure.

5. Fixed bug that made it impossible to record FullFrameImages.

6. Created new image calibration files for good results at 1024x768 
   and 1296x972.

7. Added version number at the top of the PiVR window.

8. Added Timelapse option. 

9. Fixed bug where time was indicated incorrectly when doing post-hoc 
   tracking.

10. Added 'undistorted' to video files that have gone through the 
    undistort process.

11. Fixed error in issue #50: Too many frames when converting video.

12. Added version info, git branch and git hash to each 
    experiment_settings.json file. As it's possible to record with one 
    version but track with another, there are two strings, one 
    'recording' and the other 'tracking'.

13. Added shutter speed to 'experiment_settings.json' if doing online 
    tracking.

14. Added git branch and hash to 'About/Version' to allow easy checking 
    of branch and hash.

15. Changed pathname of GerberFile

## 1.5.1
### 10th of June, 2021

1. In Tools -> Analysis the 'Distance to Source' algorithm applied a 
   median filter with window size 3 before calculating distance to 
   source. This has been removed for clarity. Users must implement 
   their own filtering.

2. In Tools -> Display Tracked Experiment a new option to manually 
   correct head/tail swaps has been added. Described in the manual.

3. For unknown reasons one of the resolutions was set to 1024x784
   instead of the normally used 1024x764. Changed it everywhere to 
   1024x764.
   
4. It is now possible to select correctly formatted VR arenas at 
   resolutions 1024x764 and 1296x972 (previously only 640x480 
   possible). 

5. Fixed bug: When recording a video and then doing post-hoc 
   tracking there are two data.csv files. When doing 'Display 
   Tracked Experiment' it will now take the newest csv file.
   
6. Post hoc HT swap: data.csv of experiment with time dependent 
   stimulus does not contain 'stimulation' column but instead 'Stim 
   Ch1' to 'Stim Ch4'. Fixed this in visualize_tracked_experiment.py.
   
7. Post hoc single animal tracking: At high frame rates the animal 
   detection algorithm sometimes had problems detecting the animal 
   as it wouldn't move much between frames while the video encoder 
   would introduce artifacts which would be wrongly detected as the 
   animal. Fixed by setting the time the detection algorithm starts 
   to look for the animal to 1 second after the start. 
   
8. Updated HP version BOM as the LEDs were missing in the csv file.

## 1.5.0
### 27th of March, 2021

1. Enable higher resolution closed loop tracking. Tested 1024x784 & 
   1296x972. This is *only* for tracking, virtual realities are 
   currently not supported other than 640*480.  
   
2. Removed 2592x1944 as it threw a 'out of resources' error.

3. Updated website to reflect changes, including some stats on 
   achieved fps. 

## 1.4.20
### 11th of November, 2020

1. Acknowledge user who suggest improvements.

## 1.4.19
### 11th of November, 2020

1. Added Schilder Lab to PiVR users

2. Added additional instructions on how to solder HP LEDs attached to
   aluminum board (Thanks to input from Rudd).

## 1.4.18
### 26th of October, 2020

1. Fixed bug described in #45: When recording video with a time
   dependent stimulus file shorter than the recording time the 
   the frame number wasn't being updated

## 1.4.17
### 24th of October, 2020

1. Added missing 'MiniPuck' to the BOM of the HP PiVR version

## 1.4.16
### 21st of October, 2020

1. Updated install instructions to work with MacIOS to fix #41.

2. Removed install instructions from README.MD - everything is on
   readthedocs now.
   
## 1.4.15
### 10th of October, 2020

1. In 'Analysis->Distance to Source' bug with different video length.
   Fixed
   
2. Bug on RPi when recording long videos at high framerate (#39).
   Fixed
   
3. In 'Display Tracked Experiment' the 'Swap Head Tail' button was
   broken. Fixed   

## 1.4.14
### 1st of October, 2020

1. Updated missing links

## 1.4.13
### 25th of September, 2020

1. Added link to Scientific American article

## 1.4.12
### 7th of August, 2020

1. Added 'Community Building' tab on website

## 1.4.11
### 14th of July, 2020

1. Added link on main page to the PLOS Bio methods paper.

## 1.4.10
### 4th of July, 2020

1. Added chapter on how to build the high powered LED PiVR version.

## 1.4.9
### 12th of June, 2020

1. Added chapter on how PiVR presents dynamic virtual realities on
   the webpage.

## 1.4.8
### 25th of April, 2020

1. Update software can now be accessed on the PC as well. Only tested
   on Windows for now. 

## 1.4.7
### 24th of April, 2020

1. Added option in visualize_tracked_experiment.py to create video
   that indicates on the top right when exactly a time dependent
   stimulus was delivered. 
   
2. Also updated the website to reflect these changes.

3. Added error messages when user is selecting problematic files as
   virtual arena and turned of the 'update vr arena' entry unles a npy
   file is being used.

## 1.4.6
### 24th of April, 2020

1. Fixed bug where distance to source VR analysis claimed to show
    distance in mm while it was shown in pixels.

## 1.4.5
### 11th of March, 2020

1. Updated install instruction for Mac to get ffmpeg installed correctly

## 1.4.4
### 11th of March, 2020

1. Added paragraph on how to create a dynamic virtual reality to be
   used with PiVR in 'tools'
   
## 1.4.3
### 10th of March, 2020

1. Added the new (shorter) version of the Building PiVR on index page.
   Also added a copy on 'Build your own' for easy referencing

## 1.4.2
### 9th of March, 2020

1. Added time lapse video of how to build PiVR on the index page of
   the websie
2. Had to change the requirement.txt for building the website
   : matplotlib == 3.0.3

## 1.4.1
### 11th of February, 2020

1. Fixed missing requirement that lead to website building failings.

## 1.4.0
### 11th of February, 2020

1. New option under image_data_handling: Undistort videos. Enables 
   user to automatically remove lens distortion of 1 or many folders.
2. User can choose codec (currently only h264 and unencoded) when doing
   undistort/convert video
3. Added info above to website
4. Added several points to FAQ on website
5. Updated README.MD and website to include opencv2 and ffmpeg on 
   PC installation. TODO: Update autoinstall files for Windows and 
   Linux 

## 1.3.15
### 27th of January, 2020

1. Bugfix: When doing Analysis>Distance to source, VR with a single
   folder an error was thrown due to incorrect path handling. Fixed  

## 1.3.14
### 17th of January, 2020

1. On landing page of the website, added link to preprint.

## 1.3.13
### 29th of December, 2019

1. On landing page of the website, added description and video of
   dynamic VR

## 1.3.12
### 20th of December, 2019

1. Added Bill of Materials in Table format onto the website.

## 1.3.11
### 18th of December, 2019

1. Added license info to About/Version tab

## 1.3.10
### 17th of December, 2019

1. Fixed a ton of things on the website and in some docstrings.

## 1.3.9
### 3rd of December, 2019

1. Added Ubunutu environemnt installtion file #2 as the original didn't work

## 1.3.8
### 2nd of December, 2019

1. Added Ubunutu environemnt installtion file #2 as the original didn't work

## 1.3.7
### 2nd of December, 2019

1. Added link to Source code for analysis script on website/manual

## 1.3.6
### 27th of November, 2019

1. Fixed installation files (CR -> LF)

## 1.3.5
### 26th of November, 2019

1. Fixed width of images on home page on main page


## 1.3.4
### 26th of November, 2019

1. Moved repository from my personal space to the louislab space.
   Updated links - I *think* they should work.

## 1.3.3
### 25th of November, 2019

1. Moved repository from my personal space to the louislab space.
   Updated links - I *think* they should work. 

## 1.3.2
### 25th of November, 2019

1. MultiAnimalTracker is a bit less memory intensive - the STD is
   calculated only once (instead for each image). Images shown in popup
   window are already smoothened.

## 1.3.1
### 24th of November, 2019

1. Improve looks of changelog

## 1.3.0
###  24th of November, 2019

1. The timestamps during tracking are now taken from the GPU and are
   real-time timestamps. To get this data, it was necessary to rewrite
   the a lot of the code in the fast_tracking.py module
   
2. Virtual reality arena and time dependent stimulus are now floating
   point from 0 to 100 (instead of uint16 from 0-65535 for VR arenas and
   0-40000 for time dependent stimulus)
   
3. Lots of documentation was added, both in docs/build and as docstring
   in function/classes

4. Harmonize frame grabbing in pre_experiment.py module: instead of
   calling the RGBtoArray function, use the YUVtoArray function.
   
5. Save more data in the experiment_settings.json, including dutycycle
   for each channel and GPIO for each channel
   
6. Added this changelog file

7. Cleaned up the GUI, both on the RPi and on PC. 
