# PiVR - virtual reality for small animals 

PiVR is a virtual reality system for small animals with a dedicated 
website: www.PiVR.org 

It creates virtual realities by detecting the position of an animal in 
real space (left) and depending on the position of the animal in 
virtual space (center) presents the appropriate stimulus (right). 

![Creating_VR](https://gitlab.com/LouisLab/pivr/-/raw/master/PiVR/pics/landing_page/Creating_VR.gif)

PiVR has been used with a variety of animals, including fruit fly larva, 
walking flies and zebrafish larvae. 

|Drosophila larva in virtual odor reality|Adult fly in virtual gustatory reality|Zebrafish under virtual lightbulb|
|---|---|---|
|![Larva Volcano](https://gitlab.com/LouisLab/pivr/-/raw/master/PiVR/pics/landing_page/larva_donut.gif)|![Fly_bitter.gif](https://gitlab.com/LouisLab/pivr/-/raw/master/PiVR/pics/landing_page/Fly_bitter.gif)|![Fish_light.gif](https://gitlab.com/LouisLab/pivr/-/raw/master/PiVR/pics/landing_page/Fish_light.gif)|
|![larva_volcano_DST.jpg](https://gitlab.com/LouisLab/pivr/-/raw/master/PiVR/pics/landing_page/larva_volcano_DST.jpg)|![fly_bitter_PI.jpg](https://gitlab.com/LouisLab/pivr/-/raw/master/PiVR/pics/landing_page/fly_bitter_PI.jpg)|![fish_light_DST.jpg](https://gitlab.com/LouisLab/pivr/-/raw/master/PiVR/pics/landing_page/fish_light_DST.jpg)|
|Drosophila larva are very efficient in staying close to the rim of the donut shaped virtual odor source|Flies are strongly repelled by the virtual bitter taste by modulating movement speed.|Zebrafish larvae are able to stay under a virtual light bulb by exclusively integrating temporal light intensity information|

## Publication

PiVR is described in detail in our recent publication: 
https://doi.org/10.1371/journal.pbio.3000712

Scientific American has written an article describing our work:
https://www.scientificamerican.com/article/fruit-flies-plug-into-the-matrix/
This includes a video: https://youtu.be/71SI7UScBU4 

## Documentation 

For examples what the system has been used for, how it might be useful 
for you and how to build your own tracking system please head over 
to www.PiVR.org. 
You will also find installation instructions there.

In August 2021 we presented PiVR at a World Wide Open Source Seminar: 
https://youtu.be/uG898FL421U
