*Pick And Place List
*Company=
*Author=
*eMail=
*
*Project=Diagram_v2.5-current_Control
*Date=16:32:56
*CreatedBy=Fritzing 0.9.8b.2021-08-03.CD-92-0-b3d4fefa
*
*
*Coordinates in mm, always center of component
*Origin 0/0=Lower left corner of PCB
*Rotation in degree (0-360, math. pos.)
*
*No;Value;Package;X;Y;Rotation;Side;Name
1;;THT;75.2995;-38.5573;0;Bottom;+LCD 5V-
2;;;9.89983;-44.8956;0;Bottom;IMG1
3;;;74.3054;-20.0436;0;Bottom;Via22
4;;Pinned Version;10.7587;-26.9067;0;Bottom;MiniPuck13
5;;Pinned Version;35.9614;-26.7453;0;Bottom;MiniPuck27
6;;;29.5328;-38.8199;0;Bottom;TXT4
7;;;76.8268;-8.08105;0;Bottom;Via20
8;;;29.6884;-37.4947;0;Bottom;TXT3
9;;;72.2376;-40.6744;0;Bottom;Via3
10;;;76.7996;-3.05735;0;Bottom;Hole2
11;;power_jack_slot;39.0401;-6.48084;0;Bottom;12V/17
12;;;3.29953;-51.5573;0;Bottom;Hole4
13;;;52.3028;-15.563;0;Bottom;Via18
14;;;38.8284;-38.5988;0;Bottom;Via15
15;;TO220 [THT];60.2596;-42.5813;0;Bottom;GPIO18
16;;;62.7831;-43.5675;0;Bottom;Via28
17;;;76.7996;-51.5573;0;Bottom;Hole1
18;;;45.3043;-3.54433;0;Bottom;Via23
19;;power_jack_slot;25.7418;-6.43173;0;Bottom;12V/27
20;;;39.7916;-4.06559;0;Bottom;Via34
21;;power_jack_slot;67.6524;-7.84191;0;Bottom;12V IN
22;;;74.3153;-4.54452;0;Bottom;Via32
23;;;49.2935;-33.0509;0;Bottom;Via29
24;10k;THT;43.3796;-41.0573;0;Bottom;R18
25;;;16.8074;-47.0465;0;Bottom;Via27
26;;;3.29953;-3.05735;0;Bottom;Hole3
27;;;49.2983;-38.5743;0;Bottom;Via24
28;;Pinned Version;60.4614;-26.7453;0;Bottom;MiniPuck17
29;;;29.3496;-42.2516;0;Bottom;TXT1
30;;power_jack_slot;12.5402;-6.48084;0;Bottom;12V/13
31;;THT;43.7694;-49.7875;180;Bottom;RPi GPIO
32;;power_jack_slot;52.7417;-6.43173;0;Bottom;12V/18
33;;;29.539;-40.3659;0;Bottom;TXT2
34;;;57.7906;-36.5494;0;Bottom;Via31
35;;;76.8268;-26.0806;0;Bottom;Via19
36;;;18.7995;-3.05735;0;Bottom;Hole5
37;;;21.2739;-44.5673;0;Bottom;Via13
