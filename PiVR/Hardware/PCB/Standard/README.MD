# Standard PCB

Depending on the type of LED you want to use, you can either build a 
Standard PiVR (this folder) or a High Power LED setup (other folder).

Depending on where you order your PCB from you have different formats
 of the PCB to choose from. 
 
I have always used https://aisler.net/, a company that accepts the Fritzing 
file (*.fzz). Other companies will ask for Gerber or other files.

If you need a particular format which you can not find here, please 
open an 'Issue': 
https://gitlab.com/davidtadres/pivr/issues

 