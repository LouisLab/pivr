__author__ = 'David Tadres'
__project__ = 'PiVR'

import os
import tkinter as tk

import imageio
import numpy as np
from PIL import Image, ImageTk
from pathlib import Path

# todo - rename this module to a more generic name - it seems that
#  I'll put all the classes that pull up a new window here

# this try-except statement checks if the processor is a ARM processor
# (used by the Raspberry Pi) or not.
# Since this command only works in Linux it is caught using
# try-except otherwise it's throw an error in a Windows system.
try:
    if os.uname()[4][:3] == 'arm':
        # This will yield True for both a Raspberry and for M1 Chip
        # Apple devices.
        # Use this code snippet
        # (from https://raspberrypi.stackexchange.com/questions/5100/detect-that-a-python-program-is-running-on-the-pi)
        import re
        CPUINFO_PATH = Path("/proc/cpuinfo")
        if CPUINFO_PATH.exists():
            with open(CPUINFO_PATH) as f:
                cpuinfo = f.read()
            if re.search(r"^Model\s*:\s*Raspberry Pi", cpuinfo, flags=re.M) is not None:
                # if True, is Raspberry Pi
                RASPBERRY = True
                LINUX = True
        else: # Test if one more intendation necessary or not. On Varun's computer
            # is Apple M1 chip (or other Arm CPU device).
            RASPBERRY = False
            LINUX = True
    else:
        # is either Mac or Linux
        RASPBERRY = False
        LINUX = True

    DIRECTORY_INDICATOR = '/'
except AttributeError:
    # is Windows
    RASPBERRY = False
    LINUX = False
    DIRECTORY_INDICATOR = '\\'

if RASPBERRY:
    from picamera.array import PiRGBArray


#touchscreen_resolution = [800,480]
#resolution = [640, 480]
#resolution = [1024, 768]

class DistanceConfigurationLive():
    """
    In order for the program to know how many pixel in a image taken
    translate to a given distance the user needs to provide some
    information.
    The user needs to provide an object with a known length in mm.
    This objects needs to be placed in front of the camera. When
    calling this function the user has to enter the length of the
    object and, using sliders, adjust the image seen so that the
    edges of the image are at the same place as the edge of the
    object.

    Note: There is currently not "Cancel" option. Might be
    implemented in future, but be careful as this function is called
    all over the script and might lead to unexpected errors if user
    cancels!
    """

    # The class take the camera object, the current resolution and a
    # previous value for the known distance. If the user never entered
    # the distance before it will be 1
    def __init__(self, cam = None, resolution = None, known_distance = None):

        # take the input and define variables from them
        self.camera = cam
        self.resolution = resolution
        print(self.resolution)
        self.known_distance = known_distance

        # the on/off switch for the looping function (dist_config)
        self.run = True

        # some variables to construct the window
        self.image_height = 100
        self.height_slider = 60
        self.height_buttons = 30

        # create a new window and name it
        self.child = tk.Toplevel()
        self.child.wm_title('Distance Configuration for resolution: '
                            + repr(self.resolution))

        #disable main window
        self.child.grab_set()

        # create a frame in the window and adjust the size.
        # To keep code as flexible as possible just check for the width of
        # currently active resolution. If smaller than 1000 show full length.
        # elif smaller than 1500 show at half the size
        # else (max is 2500) show at one third the size
        #if self.resolution == [640,480]:
        if self.resolution[0] < 1000:
            self.child_frame = tk.Frame(
                self.child,
                width = self.resolution[0],
                height = self.image_height + 2*self.height_slider
                         + 2*self.height_buttons)
        #elif self.resolution == [1024, 768] or self.resolution == [1296, 972]:
        elif self.resolution[0] < 1500:
            self.child_frame = tk.Frame(
                self.child,
                width = self.resolution[0]/2,
                height = self.image_height + 2*self.height_slider
                         + 2*self.height_buttons)
        else:
            self.child_frame = tk.Frame(
                self.child,
                width = self.resolution[0]/3,
                height = self.image_height + 2*self.height_slider
                         + 2*self.height_buttons)

        self.child_frame.pack()

        # the variable we'll need for the slider
        self.size_left = None
        # construction of the slider for the left side. Is on top.
        # Slider has the size of the resolution of the image
        #if self.resolution == [640, 480]:
        if self.resolution[0] < 1000:
            self.size_scale_left= tk.Scale(
                self.child,
                from_=0,
                to=self.resolution[0],
                resolution=1,
                label = 'Left cutoff',
                variable=self.size_left,
                orient='horizontal',
                len = self.resolution[0])
        #elif self.resolution == [1024, 768] or self.resolution == [1296, 972]:
        elif self.resolution[0] < 1500:
            self.size_scale_left= tk.Scale(
                self.child,
                from_=0,
                to=self.resolution[0],
                resolution=1,
                label = 'Left cutoff',
                variable=self.size_left,
                orient='horizontal',
                len = self.resolution[0]/2)
        else:
            self.size_scale_left= tk.Scale(
                self.child,
                from_=0,
                to=self.resolution[0],
                resolution=1,
                label = 'Left cutoff',
                variable=self.size_left,
                orient='horizontal',
                len = self.resolution[0]/3)

        # the slider is set to 0 to show the whole picture
        self.size_scale_left.set(0)
        # and it's placed on the top left
        self.size_scale_left.place(x=0,y=0)

        # then a empty canvas is created. The image will be shown here
        self.child_canvas = tk.Canvas(
            self.child_frame,
            width = self.resolution[0],
            height = self.image_height)
        # this is located just below the top slider
        self.child_canvas.place(x=0,
                                y=self.height_slider)

        # similar to the slider above. Difference is that the image
        # is cut from the right side.
        # Therefore the slider is at the width of the image in order
        # to show the image.
        # To change the resolution of the slider, just change the
        # 'resolution =1' to something else
        self.size_right = None
        #if self.resolution == [640,480]:
        if self.resolution[0] < 1000:
            self.size_scale_right= tk.Scale(
                self.child,
                from_=0,
                to=self.resolution[0],
                resolution=1,
                label = 'Right cuttoff',
                variable=self.size_right,
                orient='horizontal',
                len = self.resolution[0])
        #elif self.resolution == [1024, 768] or self.resolution == [1296, 972]:
        elif self.resolution[0] < 1500:
            self.size_scale_right= tk.Scale(
                self.child,
                from_=0,
                to=self.resolution[0],
                resolution=1,
                label = 'Right cuttoff',
                variable=self.size_right,
                orient='horizontal',
                len = self.resolution[0]/2)
        else:
            self.size_scale_right = tk.Scale(
                self.child,
                from_=0,
                to=self.resolution[0],
                resolution=1,
                label='Right cuttoff',
                variable=self.size_right,
                orient='horizontal',
                len=self.resolution[0] / 3)

        self.size_scale_right.set(self.resolution[0])
        self.size_scale_right.place(x=0,
                                    y=self.image_height+self.height_slider)

        # The label that asks the user to input the known length in
        # mm. If a length has been entered in the past use that
        self.length_label = tk.Label(self.child,
                                     text='Enter length in mm')
        self.length_label.place(x=0,
                                y = self.image_height
                                    + self.height_slider
                                    + self.height_slider)
        self.length = tk.Entry(self.child)
        self.length.place(x=0,
                          y = self.image_height + self.height_slider
                              + self.height_slider + self.height_buttons)
        self.length.insert(tk.END, repr(known_distance))

        # Display the currently selected distance between the sliders
        self.pixel_label = tk.Label(self.child,
                                    text = 'The distance in pixel')
        self.pixel_label.place(x=150,
                               y=self.image_height
                                 + self.height_slider
                                 + self.height_slider)
        self.pixel = tk.Label(self.child,
                              height = 1,
                              width = 5,
                              text = repr(self.resolution[0]))
        self.pixel.place(x=150,
                         y= self.image_height + self.height_slider
                            + self.height_slider + self.height_buttons)

        # Display the pixel per mm
        self.pixel_per_mm_label = tk.Label(self.child,
                                           text = 'pixel / mm')
        self.pixel_per_mm_label.place(x = 300,
                                      y = self.image_height
                                          + self.height_slider
                                          + self.height_slider)
        self.pixel_per_mm = tk.Label(self.child,
                                     height = 1,
                                     width = 15,
                                     text = repr(20/self.resolution[0]))
        self.pixel_per_mm.place(x=300,
                                y=self.image_height
                                  + self.height_slider
                                  + self.height_slider
                                  + self.height_buttons)

        # To quit the program we need a button
        self.quit_button = tk.Button(self.child,
                                     text = 'Quit',
                                     fg = 'red',
                                     command = self.quit)
        self.quit_button.place(x=400,
                               y = self.image_height
                                   + self.height_slider
                                   + self.height_slider
                                   + self.height_buttons)

        # Then the tkinter loop needs to call this function
        self.running = self.dist_config()

    def dist_config(self):
        # run until self.run is not True anymore, which will happen when the user presses the quit button.
        while self.run:
            # grab a frame using the video port
            with PiRGBArray(self.camera) as output:
                self.camera.capture(output,'rgb', use_video_port = True)
                # no need for a real copy of the image (using .copy() )
                current_frame = output.array[:,:,0]
            # depending on the resolution the program displays:
            #if self.resolution == [640,480]:
            if self.resolution[0] < 1000:
                # all the pixels in the width of the image. It will only display the center +/- the image_height/2 defined
                # above
                self.photo_local = ImageTk.PhotoImage(image=Image.fromarray(
                    current_frame[int((self.resolution[1]/2)-(self.image_height/2)):int((self.resolution[1]/2)+(self.image_height/2)),
                                  self.size_scale_left.get():self.size_scale_right.get()]))
            #elif self.resolution == [1024, 768] or self.resolution == [1296, 972]:
            elif self.resolution[0] < 1500:
                # only every second pixel in the width of the image. Same height definition as for the 640x480
                self.photo_local = ImageTk.PhotoImage(image=Image.fromarray(
                    current_frame[int((self.resolution[1]/2)-(self.image_height)):int((self.resolution[1]/2)+(self.image_height)):2,
                                  self.size_scale_left.get():self.size_scale_right.get():2]))
            else:
                # only every second pixel in the width of the image. Same height definition as for the 640x480
                self.photo_local = ImageTk.PhotoImage(image=Image.fromarray(
                    current_frame[int((self.resolution[1]/2)-(self.image_height)):int((self.resolution[1]/2)+(self.image_height)):2,
                                  self.size_scale_left.get():self.size_scale_right.get():3]))

            # display the above defined image in the prepared canvas
            self.child_canvas.create_image(0, 0,
                                           image=self.photo_local, anchor=tk.NW)
            # and update the text box that says how many pixels we currently have in displayed in width dimension
            self.pixel.configure(text = repr(self.size_scale_right.get()-self.size_scale_left.get()))

            # Sometimes there are floating point division errors, e.g. if the user enters '0'. Therefore the try except statement
            try:
                # calclate the number of pixels shown / user given length of object in mm
                self.distance_factor = (self.size_scale_right.get()-self.size_scale_left.get()) / \
                                       int(self.length.get())
                # display this number in the provided text box
                self.pixel_per_mm.configure(text = repr(self.distance_factor)[0:10])
                # save the distance in mm that the user entered for future use
                self.known_distance = int(self.length.get())
            except:
                # in case of floating point error just pass and wait until the user enters a number that can be used
                pass

            # update the window as fast as possible
            self.child.update()

    def quit(self):
        # stop the dist_config loop
        self.run = False
        # set main window active again
        self.child.grab_release()
        # close the child window
        self.child.after(0, self.child.destroy())


class DistanceConfigurationStatic():
    """
    In order for the program to know how many pixel in a image taken
    translate to a given distance the user needs to provide some
    information.

    The user needs to open an image that shows an object of known
    length/diameter in mm. The PiVR system must have taken that image
    with the same distance as the experiment that should be analyzed.
    A petri dish with a known diameter will work just fine.

    When calling this function the user has to enter the length of
    the object and, using sliders, adjust the image seen so that the
    edges of the image are at the same place as the edge of the
    object.

    Note: There is currently not "Cancel" option. Might be
    implemented in future, but be careful as this function is called
    all over the script and might lead to unexpected errors if user
    cancels!
    """

    def __init__(self, known_distance):

        self.known_distance = known_distance

        # the on/off switch for the looping function (dist_config)
        self.run = True

        # some variables to construct the window
        self.image_height = 100
        self.height_slider = 60
        self.height_buttons = 30

        # grab an image that will have the same pixel per mm as the data that should be analyzed
        self.standard_image_path = tk.filedialog.askopenfilename(
            title="Select file",filetypes=(("jpeg files", "*.jpg;*.jpeg"),
                                              ("png files", "*.png"),
                                              ("tiff files", "*.tiff;*.tif"),
                                              ("h264 files", "*.h264"),
                                              ("numpy files", "*.npy"),
                                              ("all files", "*.*")))

        if 'npy' in self.standard_image_path.split('.')[-1]:
            self.standard_image = np.load(self.standard_image_path)
            self.standard_image = self.standard_image[:,:,0]
        else:
            try:
                self.standard_image = imageio.imread(self.standard_image_path)
            except ValueError: # If video
                video_object = imageio.get_reader(
                    self.standard_image_path, 'ffmpeg')
                self.standard_image = video_object.get_data(0)

        print(self.standard_image_path.split('.')[-1])

        # get resolution
        self.resolution = [self.standard_image.shape[1],
                           self.standard_image.shape[0]]

        # create a new window and name it
        self.child = tk.Toplevel()
        self.child.wm_title('Distance Configuration for resolution: '
                            + repr(self.resolution))

        # disable main window
        self.child.grab_set()

        self.child_frame = tk.Frame(self.child,
                                    width=self.resolution[0],
                                    height=self.image_height
                                           + 2 * self.height_slider
                                           + 2 * self.height_buttons)

        self.child_frame.pack()

        # the variable we'll need for the slider
        self.size_left = None
        # construction of the slider for the left side. Is on top.
        # Slider has the size of the resolution of the image
        self.size_scale_left = tk.Scale(
            self.child,
            from_=0,
            to=self.resolution[0],
            resolution=1,
            label='Left cutoff',
            variable=self.size_left,
            orient='horizontal',
            len=self.resolution[0])

        # the slider is set to 0 to show the whole picture
        self.size_scale_left.set(0)
        # and it's placed on the top left
        self.size_scale_left.place(x=0, y=0)

        # then a empty canvas is created. The image will be shown here
        self.child_canvas = tk.Canvas(self.child_frame,
                                      width=self.resolution[0],
                                      height=self.image_height)
        # this is located just below the top slider
        self.child_canvas.place(x=0, y=self.height_slider)

        # similar to the slider above. Difference is that the image
        # is cut from the right side.
        # Therefore the slider is at the width of the image in order
        # to show the image.To change the resolution of the slider,
        # just change the 'resolution =1' to something else
        self.size_right = None

        self.size_scale_right = tk.Scale(self.child,
                                         from_=0,
                                         to=self.resolution[0],
                                         resolution=1,
                                         label='Right cuttoff',
                                         variable=self.size_right,
                                         orient='horizontal',
                                         len=self.resolution[0])

        self.size_scale_right.set(self.resolution[0])
        self.size_scale_right.place(x=0, y=self.image_height
                                           + self.height_slider)

        # The label that asks the user to input the known length in
        # mm. If a length has been entered in the past use that
        self.length_label = tk.Label(self.child,
                                     text='Enter length in mm')
        self.length_label.place(x=0,
                                y=self.image_height
                                  + self.height_slider
                                  + self.height_slider)
        self.length = tk.Entry(self.child)
        self.length.place(x=0,
                          y=self.image_height + self.height_slider
                            + self.height_slider + self.height_buttons)
        self.length.insert(tk.END, repr(self.known_distance))

        # Display the currently selected distance between the sliders
        self.pixel_label = tk.Label(self.child, text='The distance in pixel')
        self.pixel_label.place(x=150,
                               y=self.image_height
                                 + self.height_slider
                                 + self.height_slider)
        self.pixel = tk.Label(self.child,
                              height=1,
                              width=5,
                              text=repr(self.resolution[0]))
        self.pixel.place(x=150,
                         y=self.image_height + self.height_slider
                           + self.height_slider + self.height_buttons)

        # Display the pixel per mm
        self.pixel_per_mm_label = tk.Label(self.child,
                                           text='pixel / mm')
        self.pixel_per_mm_label.place(x=300,
                                      y=self.image_height
                                        + self.height_slider
                                        + self.height_slider)
        self.pixel_per_mm = tk.Label(self.child,
                                     height=1,
                                     width=15,
                                     text=repr(20 / self.resolution[0]))
        self.pixel_per_mm.place(x=300,
                                y=self.image_height
                                  + self.height_slider
                                  + self.height_slider
                                  + self.height_buttons)

        # To quit the program we need a button
        self.quit_button = tk.Button(self.child,
                                     text='OK',
                                     fg='green',
                                     command=self.quit)
        self.quit_button.place(x=400,
                               y=self.image_height
                                 + self.height_slider
                                 + self.height_slider
                                 + self.height_buttons)

        # Then the tkinter loop needs to call this function
        self.running = self.dist_config()


    def dist_config(self):
        # run until self.run is not True anymore, which will happen
        # when the user presses the OK button.
        while self.run:

            # all the pixels in the width of the image. It will only
            # display the center +/- the image_height/2 defined
            # above
            self.photo_local = ImageTk.PhotoImage(
                image=Image.fromarray(
                    self.standard_image[
                    int((self.resolution[1] / 2)
                        - (self.image_height / 2)):int((self.resolution[1] / 2)
                                                       + (self.image_height / 2)),
                self.size_scale_left.get():self.size_scale_right.get()]))

            # display the above defined image in the prepared canvas
            self.child_canvas.create_image(0, 0,
                                           image=self.photo_local,
                                           anchor=tk.NW)
            # and update the text box that says how many pixels we
            # currently have in displayed in width dimension
            self.pixel.configure(text=repr(self.size_scale_right.get()
                                           - self.size_scale_left.get()))

            # Sometimes there are floating point division errors,
            # e.g. if the user enters '0'. Therefore the try except
            # statement
            try:
                # calculate the number of pixels shown / user given
                # length of object in mm
                self.distance_factor = (self.size_scale_right.get()
                                        - self.size_scale_left.get()) \
                                       / int(self.length.get())
                # display this number in the provided text box
                self.pixel_per_mm.configure(
                    text=repr(self.distance_factor)[0:10])
                # save the distance in mm that the user entered for future use
                self.known_distance = int(self.length.get())
            except:
                # in case of floating point error just pass and wait
                # until the user enters a number that can be used
                pass

            # update the window as fast as possible
            self.child.update()

    def quit(self):
        # stop the dist_config loop
        self.run = False
        # set main window active again
        self.child.grab_release()
        # close the child window
        self.child.after(0, self.child.destroy())



class AnimalColorSelection():
    '''
    This window presents the user with the two options of how the signal is presented to the algorithm: dark or white.
    It also presents a picture of each option for visualization
    '''

    def __init__(self, path, linux, controller):

        # create new window and name it
        self.child = tk.Toplevel()
        self.child.wm_title('Animal Color configuration')

        #disable main window
        self.child.grab_set()

        self.controller = controller

        # TODO: # redo the images to make it clearer!!!
        if linux:
            white_image = Image.open(path + 'pics/white_animal.jpeg').resize((200,200))
            self.white_image = ImageTk.PhotoImage(white_image)
            dark_image = Image.open(path + 'pics/dark_animal.jpeg').resize((200,200))
            self.dark_image = ImageTk.PhotoImage(dark_image)
        else:
            white_image = Image.open(path + 'pics\\white_animal.jpeg').resize((200, 200))
            self.white_image = ImageTk.PhotoImage(white_image)
            dark_image = Image.open(path + 'pics\\dark_animal.jpeg').resize((200,200))
            self.dark_image = ImageTk.PhotoImage(dark_image)

        # create a frame in the window
        #self.child_frame = tk.Frame(self.child)

        white_image_button = tk.Button(self.child, text = 'My animal is white \non dark background',
                                       command=self.white_signal, bg = 'black', foreground = 'white')
        white_image_button.grid(row=0, column=0)


        dark_image_button = tk.Button(self.child, text = 'My animal is dark \non white background',
                                     command=self.dark_signal, bg = 'white')
        dark_image_button.grid(row=0, column=1)


        white_image_displayed = tk.Label(self.child, image=self.white_image)
        white_image_displayed.image = self.white_image
        white_image_displayed.grid(row=1,column=0)

        dark_image_displayed = tk.Label(self.child, image=self.dark_image)
        dark_image_displayed.image = self.dark_image
        dark_image_displayed.grid(row=1,column=1)


    def white_signal(self):
        self.controller.all_common_variables.signal = 'white'
        print(self.controller.all_common_variables.signal)
        # set main window active again
        self.child.grab_release()
        # close the child window
        self.child.after(0, self.child.destroy())

    def dark_signal(self):
        self.controller.all_common_variables.signal = 'dark'
        print(self.controller.all_common_variables.signal)
        # set main window active again
        self.child.grab_release()
        # close the child window
        self.child.after(0, self.child.destroy())





