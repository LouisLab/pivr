PiVR has been developed by David Tadres and Matthieu Louis (`Louis Lab
<https://labs.mcdb.ucsb.edu/louis/matthieu/>`__).

.. _legacy:

Legacy Options
***************

.. warning::

   This page contains information about options that are not recommended
   to be used.

   They might not be supported in future versions of PiVR. Please use
   the alternative.

.. _legacyTimeStimLabel:

Preparing a Frame Based Time Dependent Stimulus File
====================================================

.. warning::

   Please consider using a time based time dependent stimulus file as
   described :ref:`here <PrepTimeStimLabel>`.

.. figure:: Figures/manual_software/TimeDepStimFileOLD.png
   :width: 100 %
   :alt: TimeDepStimFileOLD.png

The first column (A) is the frame number. E.g. if you are recording at
30 frames per second the row 2-32 will define what’s going on in
that time.

The second column defines what Channel 1 is doing at a given frame. 0
means the light is completely OFF. 100 means the light is completely
ON. A number in between, e.g. 50 means that the light is on at
50/100=50%

The third (Channel 2), the fourth (Channel 3) and the fifth (Channel
4) use the same principle for the other channels.

It is important to notice that the stimulation file needs to be
defined on a very low level: Frame Number. The same stimulus file
will give different stimulations depending on the framerate. Therefore:

    1)	Decide on a framerate for you experiment, as an example we’ll
        say you decide on 30fps
    2)	Decide on a length of your experiment, for example 20 seconds
    3)	Decide on the stimulation pattern, e.g. you want Channel 1 to
        be OFF for the first second and
        Channel 2 to be ON for the first second. Then you want to
        switch, Channel 1 is ON for 1 sec, Channel 2 is OFF for 1 sec
    4)	You will need to set the first 30 (framerate * length of
        stimulus) rows of Channel 1 to 0
    5)	And you will need to set the first 30 (framerate * length of
        stimulus) rows of Channel 2 to 100
    6)	As you don’t care about Channel 3 and 4 you can leave it at zero
    7)	At row # 2 (since you start at row #2 in excel) or frame #
        30 (first column) you set Channel 1 to 100 for 30 rows
        (framerate * length of stimulus) to turn it ON and Channel 2
        to 0 to turn it OFF

Notes:
    A)	If you do not define enough rows for your experiment, e.g. if
        you want to run the 20 seconds experiment at 30frames per
        second but you only define what happens during the first 15
        seconds (by only going to row 15*30=450 instead of row
        20*30=600) the last value for each channel will be
        propagated, e.g. if row 450 is set to 100 and row 451 to
        600 are not defined the value 100 will be used for the rest
        of the experiment.
    B)	If you define more rows than you need for your experiments
        only the stimulation up to the point you record are used
        (this will behave as you probably expect)