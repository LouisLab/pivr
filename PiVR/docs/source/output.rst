PiVR has been developed by David Tadres and Matthieu Louis (`Louis Lab
<https://labs.mcdb.ucsb.edu/louis/matthieu/>`__).

.. _Output:

Explanation of PiVR output
**************************

.. _OutputTrackingLabel:

Tracking
========

After running a :ref:`tracking <TrackingLabel>` experiment you will
find a folder with the "DATE_TIME_EXP.GROUP" as its name. An example
would be "2019.01.11_14-00-05_CantonS". This is an experiment
conducted on the 11th of January 2019. "CantonS" is the value that
was entered in the field "Exp. Group".

This folder will contain the following files:

"DATE_TIME_data.csv"
--------------------
is probably the most important file. It contains the following data
for each frame of the experiment:

   #. The frame (=image) number into the experiment
   #. The time in seconds since the experiment started
   #. The X (column) coordinate of the Centroid (Check
      :ref:`here <OutputPointExp>` for comparison with midpoint)
   #. The Y (row) coordinate of the Centroid
   #. The X (column) coordinate of the head
   #. The Y (row) coordinate of the head
   #. The X (column) coordinate of the tail
   #. The Y (row) coordinate of the tail
   #. The X (column) coordinate of the midpoint (Check
      :ref:`here <OutputPointExp>` for comparison with centroid)
   #. The Y (row) coordinate of the midpoint
   #. The Y-min (row) coordinate of the bounding box (See
      :ref:`here <bbox_label>` for explanation
   #. The Y-max (row) coordinate of the bounding box.
   #. The X-min (row) coordinate of the bounding box.
   #. The X-max (row) coordinate of the bounding box.
   #. The local threshold used to extract the binary image during tracking.

"Background.jpg"
-----------------
contains the reconstructed background image. See
:ref:`here <AnimalDetectionExplanationLabel>` for explanation
where it is coming from and what it means.


"experiment_settings.json"
--------------------------
is a `json file <https://en.wikipedia.org/wiki/JSON>`_ and contains a
lot of useful experimental information:

#. Camera Shutter Speed [us]: Shutter speed in microseconds

#. Exp. Group: The string that was entered by the user during the
   experiment

#. Experiment Date and Time: exactly that

#. Framerate: The frequency at which PiVR tracked the animal

#. Model Organism: While tracking, PiVR used the parameters of this
   animal to optimize tracking. See :ref:`here<define_new_animal_label>`
   for how to modify this parameter.

#. PiVR info (recording): version number, git branch and git hash of
   the PiVR software that was used to record the experiment.

#. PiVR info (tracking): version number, git branch and git hash of
   the PiVR software that was used to track the experiment. If online
   tracking is being done, this is identical to the info above.

#. Pixel per mm: For PiVR to be able to track the animal, it needs
   to know how many pixels indicate one mm. This has been set by the
   user as described :ref:`here<PixMMLabel>`.

#. Recording time: The time in seconds that PiVR was tracking the
   animal

#. Resolution: The camera resolution in pixels that PiVR used while
   tracking.

#. Time delay due to Animal Detection[s]: For the
   :ref:`autodetection<AnimalDetectionExplanationLabel>` the animal
   must move. The time it took between pressing "start" and successful
   animal detection is saved here.

#. Virtual Reality arena name: If no virtual arena was presented,
   it will say 'None'

#. backlight 2 channel: If Backlight 2 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 18)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. backlight channel: If Backlight 1 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 18)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].
   This would normally be defined as [18, 40000].

#. output channel 1: If Channel 1 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 17)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. output channel 2: If Channel 2 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 27)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. output channel 3: If Channel 3 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 13)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. output channel 4: If Channel 4 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 13)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

"first_frame_data.json"
-----------------------
is a `json file <https://en.wikipedia.org/wiki/JSON>`__ and contains
information that collected during
:ref:`animal detection<AnimalDetectionExplanationLabel>` (Source
code :class:`pre_experiment.FindAnimal`.)

#. bounding box col max: The X_max value of the bounding box of the
   animal detected in the first frame during animal detection.

#. bounding box col min: The X_min value of the bounding box of the
   animal detected in the first frame during animal detection.

#. bounding box row max: The Y_min value of the bounding box of the
   animal detected in the first frame during animal detection.

#. bounding box row min: The Y_max value of the bounding box of the
   animal detected in the first frame during animal detection.

#. centroid col: The X value of the centroid of the animal detected
   in the first frame during animal detection.

#. centroid row: The Y value of the centroid of the animal detected
   in the first frame during animal detection.

#. filled area: The filled area in pixels of the blob defined as
   the animal in the first frame during animal detection

"sm_raw.npy"
-----------------------

is a `Numpy <https://numpy.org/>`_ file. It contains the small image.

This file comes in
`shape <https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.shape.html>`_
["y size", "x size", # of frames]. "y size" == "x size" and is defined
based on the :ref:`organism<SelectOrganismLabel>` and
:ref:`px/mm<PixMMLabel>`. Specifically, it is
2*'max_skeleton_length_mm' * pixel_per_mm to ensure the animal fits into
the array.

"sm_thresh.npy"
-----------------------

is a `Numpy <https://numpy.org/>`_ file. It contains the binarized
small image.

This file comes in
`shape <https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.shape.html>`_
["y size", "x size", # of frames]. "y size" == "x size" and is defined
based on the :ref:`organism<SelectOrganismLabel>` and
:ref:`px/mm<PixMMLabel>`. Specifically, it is
2*'max_skeleton_length_mm' * pixel_per_mm to ensure the animal fits into
the array.

"sm_skeletons.npy"
-----------------------

is a `Numpy <https://numpy.org/>`_ file. It contains the skeleton of the
tracked animal.

This file comes in
`shape <https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.shape.html>`_
["y size", "x size", # of frames]. "y size" == "x size" and is defined
based on the :ref:`organism<SelectOrganismLabel>` and
:ref:`px/mm<PixMMLabel>`. Specifically, it is
2*'max_skeleton_length_mm' * pixel_per_mm to ensure the animal fits into
the array.

"undistort_matrices.npz"
-----------------------

contains the undistort files used to :ref:`correct for lens distortion of the
image<UndistortOptionsLabel>`.

The file contains two objects, 'mtx' and 'dist' which are used by the
`cv2.undistortPoints <https://docs.opencv.org/3.4/da/d54/group__imgproc__transform.html>`_
function.

Optional files
--------------

The rest of this subsection describes files that need to be explicitly
saved as described :ref:`here<DefineOutputFilesLabel>`.

.. _bbox_label:

"bounding_boxes.npy"
~~~~~~~~~~~~~~~~~~~~~~
is a `Numpy <https://numpy.org/>`_ file. It contains the coordinates
of the bounding box of the small image. The bounding box defines the
Y/X coordinates of the small image.

This file comes in
`shape <https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.shape.html>`_
[4, # of frames] with:

======  =========================
[0, :]  contains the Y_min values
------  -------------------------
[1, :]  contains the Y_max values
------  -------------------------
[2, :]  contains the X_min values
------  -------------------------
[3, :]  contains the X_max values
======  =========================

These values are necessary to describe where in the full image
frame the small image that has been saved during the experiment is
located. The bounding box is the rectangle that contains all image
information used during this frame. Below an illustration on how
the different values are used to construct the bounding box.

.. figure:: Figures/ouput/bbox_illustration.png
  :width: 100 %
  :alt: bbox_illustration.png

.. note::

  Why Y/X and not X/Y? In image processing the convention is to
  reference points in (Rows, Columns) which translates to Y/X. The
  underlying image processing libraries work with the (Rows,
  Columns) convention. See for example `here
  <https://scikit-image.org/docs/dev/api/skimage.measure.html#skimage
  .measure.regionprops>`__.
  PiVR therefore follows this convention.

"centroids.npy"
~~~~~~~~~~~~~~~~~~~~~~
is a `Numpy <https://numpy.org/>`__ file. It contains the coordinates
of the centroid of the blob identified during the experiment.
See :ref:`here<OutputPointExp>` to see the centroid compared to the
midpoint.

The file comes in `shape <https://docs.scipy
.org/doc/numpy/reference/generated/numpy.ndarray.shape.html>`__
[# of frames, 2] with:

======  ==============================
[:, 0]  contains the centroid Y values
------  ------------------------------
[:, 1]  contains the centroid X values
======  ==============================

These values are identical to what you will find in the
"DATE_TIME_data.csv" file

"midpoints.npy"
~~~~~~~~~~~~~~~~~~~~~~
is a `Numpy <https://numpy.org/>`__ file. It contains the coordinates
of the midpoint extracted from the skeleton during the experiment.
See :ref:`here<OutputPointExp>` to see the midpoint compared to the
centroid.

The file comes in `shape <https://docs.scipy
.org/doc/numpy/reference/generated/numpy.ndarray.shape.html>`__
[# of frames, 2] with:

======  ==============================
[:, 0]  contains the midpoint Y values
------  ------------------------------
[:, 1]  contains the midpoint X values
======  ==============================

These values are identical to what you will find in the
"DATE_TIME_data.csv" file


"heads.npy"
~~~~~~~~~~~~~~~~~~~~~~
is a `Numpy <https://numpy.org/>`_ file. It contains the coordinates
of the head position :ref:`assigned during tracking<HeadTailLabel>`.

The file comes in `shape <https://docs.scipy
.org/doc/numpy/reference/generated/numpy.ndarray.shape.html>`__
[# of frames, 2] with:

======  ==========================
[:, 0]  contains the head Y values
------  --------------------------
[:, 1]  contains the head X values
======  ==========================

These values are identical to what you will find in the
"DATE_TIME_data.csv" file

"tails.npy"
~~~~~~~~~~~~~~~~~~~~~~
is a `Numpy <https://numpy.org/>`_ file. It contains the coordinates
of the tail position :ref:`assigned during tracking<HeadTailLabel>`.

The file comes in `shape <https://docs.scipy
.org/doc/numpy/reference/generated/numpy.ndarray.shape.html>`__
[# of frames, 2] with:

======  ==========================
[:, 0]  contains the tail Y values
------  --------------------------
[:, 1]  contains the tail X values
======  ==========================

These values are identical to what you will find in the
"DATE_TIME_data.csv" file

.. _OutputVRArenaLabel:

VR Arena and Dynamic VR Arena
=============================

After running a :ref:`VR Arena<VRLabel>` experiment you will
find a folder with the "DATE_TIME_EXP.GROUP" as its name. An example
would be "2019.01.11_14-00-05_CantonS". This is an experiment
conducted on the 11th of January 2019. "CantonS" is the value that
was entered in the field "Exp. Group".


This folder will contain the following files:

"DATE_TIME_data.csv"
--------------------
is probably the most important file. It contains the following data
for each frame of the experiment:

   #. The frame (=image) number into the experiment
   #. The time since the experiment started
   #. The X (column) coordinate of the Centroid (Check
      :ref:`here <OutputPointExp>` for comparison with midpoint)
   #. The Y (row) coordinate of the Centroid
   #. The X (column) coordinate of the head
   #. The Y (row) coordinate of the head
   #. The X (column) coordinate of the tail
   #. The Y (row) coordinate of the tail
   #. The X (column) coordinate of the midpoint (Check
      :ref:`here <OutputPointExp>` for comparison with centroid)
   #. The Y (row) coordinate of the midpoint
   #. The Y (row) coordinate of the midpoint
   #. The Y-min (row) coordinate of the bounding box (See
      :ref:`here <bbox_label>` for explanation
   #. The Y-max (row) coordinate of the bounding box.
   #. The X-min (row) coordinate of the bounding box.
   #. The X-max (row) coordinate of the bounding box.
   #. The local threshold used to extract the binary image during tracking.
   #. The stimulus delivered stimulus in %

"RESOLUTION_NAME.csv"
---------------------
for example "640x480_checkerboard.csv". This is the virtual arena
presented to the animal. In case the virtual arena is positioned
relative to the starting position and the movement of the animal
(such as the "640x480_gaussian_centred_animal_pos[250,240,0.0].csv"
arena), this file will *final* translated and rotated arena as it was
presented to the animal.

.. note::
   If a dynamic virtual reality has been presented, this file will
   not be present - it would simply take too long and take up too
   much space. This is one reason why dynamic virtual realities can
   not be translated and rotated at the moment.

"Background.jpg"
-----------------
contains the reconstructed background image. See
:ref:`here <AnimalDetectionExplanationLabel>` for explanation
where it is coming from and what it means.

"experiment_settings.json"
--------------------------
is a `json file <https://en.wikipedia.org/wiki/JSON>`_ and contains a
lot of useful experimental information:

#. Search box size: The Search box used to locate the animal during
   the experiment

#. Exp. Group: The string that was entered by the user during the
   experiment

#. Experiment Date and Time: exactly that

#. Framerate: The frequency at which PiVR tracked the animal

#. Model Organism: While tracking, PiVR used the parameters of this
   animal to optimize tracking. See **Todo** here for how to modify
   this parameter.

#. Pixel per mm: For PiVR to be able to track the animal, it needs
   to know how many pixels indicate one mm. This has been set by the
   user as described :ref:`here<PixMMLabel>`.

#. Recording time: The time in seconds that PiVR was tracking the
   animal

#. Resolution: The camera resolution in pixels that PiVR used while
   tracking.

#. Time delay due to Animal Detection[s]: For the
   :ref:`autodetection<AnimalDetectionExplanationLabel>` the animal
   must move. The time it took between pressing "start" and successful
   animal detection is saved here.

#. Virtual Reality arena name: As no virtual arena was presented,
   it will say 'None'

#. backlight 2 channel: If Backlight 2 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 18)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. backlight channel: If Backlight 1 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 18)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].
   This would normally be defined as [18, 40000].

#. output channel 1: If Channel 1 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 17)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. output channel 2: If Channel 2 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 27)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. output channel 3: If Channel 3 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 13)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. output channel 4: If Channel 4 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 13)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

"first_frame_data.json"
-----------------------
is a `json file <https://en.wikipedia.org/wiki/JSON>`_ and contains
information that collected during
:ref:`animal detection<AnimalDetectionExplanationLabel>` (Source
code :class:`pre_experiment.FindAnimal`.)

#. bounding box col max: The X_max value of the bounding box of the
   animal detected in the first frame during animal detection.

#. bounding box col min: The X_min value of the bounding box of the
   animal detected in the first frame during animal detection.

#. bounding box row max: The Y_min value of the bounding box of the
   animal detected in the first frame during animal detection.

#. bounding box row min: The Y_max value of the bounding box of the
   animal detected in the first frame during animal detection.

#. centroid col: The X value of the centroid of the animal detected
   in the first frame during animal detection.

#. centroid row: The Y value of the centroid of the animal detected
   in the first frame during animal detection.

#. filled area: The filled area in pixels of the blob defined as
   the animal in the first frame during animal detection

Optional files
--------------

The rest of this subsection describes files that need to be explicitly
saved as described here. TODO LINK

"bounding_boxes.npy"
~~~~~~~~~~~~~~~~~~~~~~
is a `Numpy <https://numpy.org/>`_ file. It contains the coordinates
of the bounding box of the small image. The bounding box defines the
Y/X coordinates of the small image

This file comes in
`shape <https://docs.scipy.org/doc/numpy/reference/generated/numpy
.ndarray.shape.html>`__
[4, # of frames] with:

======  =========================
[0, :]  contains the Y_min values
------  -------------------------
[1, :]  contains the Y_max values
------  -------------------------
[2, :]  contains the X_min values
------  -------------------------
[3, :]  contains the X_max values
======  =========================

These values are necessary to describe where in the full image
frame the small image that has been saved during the experiment is
located. The bounding box is the rectangle that contains all image
information used during this frame. Below an illustration on how
the different values are used to construct the bounding box.

.. figure:: Figures/ouput/bbox_illustration.png
  :width: 100 %
  :alt: bbox_illustration.png

.. note::

  Why Y/X and not X/Y? In image processing the convention is to
  reference points in (Rows, Columns) which translates to Y/X. The
  underlying image processing libraries work with the (Rows,
  Columns) convention. See for example `here
  <https://scikit-image.org/docs/dev/api/skimage.measure.html#skimage
  .measure.regionprops>`__.
  PiVR therefore follows this convention.

"centroids.npy"
~~~~~~~~~~~~~~~~~~~~~~
is a `Numpy <https://numpy.org/>`_ file. It contains the coordinates
of the centroid of the blob identified during the experiment.
See :ref:`here<OutputPointExp>` to see the centroid compared to the
midpoint.

The file comes in `shape <https://docs.scipy
.org/doc/numpy/reference/generated/numpy.ndarray.shape.html>`__
[# of frames, 2] with:

======  ==============================
[:, 0]  contains the centroid Y values
------  ------------------------------
[:, 1]  contains the centroid X values
======  ==============================

These values are identical to what you will find in the
"DATE_TIME_data.csv" file

"midpoints.npy"
~~~~~~~~~~~~~~~~~~~~~~
is a `Numpy <https://numpy.org/>`_ file. It contains the coordinates
of the midpoint extracted from the skeleton during the experiment.
See :ref:`here<OutputPointExp>` to see the midpoint compared to the
centroid.

The file comes in `shape <https://docs.scipy
.org/doc/numpy/reference/generated/numpy.ndarray.shape.html>`__
[# of frames, 2] with:

======  ==============================
[:, 0]  contains the midpoint Y values
------  ------------------------------
[:, 1]  contains the midpoint X values
======  ==============================

These values are identical to what you will find in the
"DATE_TIME_data.csv" file

"heads.npy"
~~~~~~~~~~~~~~~~~~~~~~
is a `Numpy <https://numpy.org/>`_ file. It contains the coordinates
of the head position :ref:`assigned during tracking<HeadTailLabel>`.

The file comes in `shape <https://docs.scipy
.org/doc/numpy/reference/generated/numpy.ndarray.shape.html>`__
[# of frames, 2] with:

======  ==========================
[:, 0]  contains the head Y values
------  --------------------------
[:, 1]  contains the head X values
======  ==========================

These values are identical to what you will find in the
"DATE_TIME_data.csv" file

"tails.npy"
~~~~~~~~~~~~~~~~~~~~~~
is a `Numpy <https://numpy.org/>`_ file. It contains the coordinates
of the tail position :ref:`assigned during tracking<HeadTailLabel>`.

The file comes in `shape <https://docs.scipy
.org/doc/numpy/reference/generated/numpy.ndarray.shape.html>`__
[# of frames, 2] with:

======  ==========================
[:, 0]  contains the tail Y values
------  --------------------------
[:, 1]  contains the tail X values
======  ==========================

These values are identical to what you will find in the
"DATE_TIME_data.csv" file

"stimulation.npy"
~~~~~~~~~~~~~~~~~
is a `Numpy <https://numpy.org/>`_ file. It contains the stimulus
delivered to the animal during the experiment.

These values are identical to what you will find in the
"DATE_TIME_data.csv" file

Full Frame Recording
====================

After taking a lot of images with
:ref:`Full Frame Recording<FullFrameLabel>`, find a folder with the
"DATE_TIME_EXP_GROUP" as its name. An example would be
"2019.01.11_14-00-05_CantonS". This is an experiment conducted on the
11th of January 2019. "CantonS" is the value that was entered in the field
"Exp. Group".

"DATE_TIME_timestamps" OR "DATE_TIME_stimulation_presented.csv"
--------------------

If no time dependent stimulus has been presented, you will find a
"DATE_TIME_timestamps.csv" file in the folder containing:

   #. Frame (=image) number into the experiment
   #. Time in seconds since the experiment started

If a time dependent stimulus was presented, you will find a
"DATE_TIME_stimulation_presented.csv" file in the folder containing:

   #. Frame (=image) number into the experiment
   #. Time in seconds since the experiment started
   #. Channel 1 stimulus delivered
   #. Channel 2 stimulus delivered
   #. Channel 3 stimulus delivered
   #. Channel 4 stimulus delivered

Image files
------------

Usually lots upon lots of them. Each image is saved separately directly
into this folder.

"experiment_settings.json"
--------------------------
is a `json file <https://en.wikipedia.org/wiki/JSON>`_ and contains a
lot of useful experimental information:

#. Experiment Date and Time: Exactly as advertised

#. Framerate: The framerate the video was recorded in

#. Exp. Group: The string that was entered by the user during the
   experiment

#. Model Organism: If selected, what animal has been indicated
   during the experiment.

#. Pixel per mm: If defined (see :ref:`here<PixMMLabel>`) a useful
   parameter for analysis.

#. Recording time: The time in seconds that PiVR was recording
   this video.

#. Resolution: The camera resolution in pixels that PiVR used while
   recording the video.

#. Virtual Reality arena name: As no virtual arena was presented,
   it will say 'None'

#. backlight 2 channel: If Backlight 2 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 18)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. backlight channel: If Backlight 1 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 18)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].
   This would normally be defined as [18, 40000].

#. output channel 1: If Channel 1 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 17)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. output channel 2: If Channel 2 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 27)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. output channel 3: If Channel 3 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 13)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. output channel 4: If Channel 4 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 13)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

(Optional) stimulation_file_used.csv
------------------------------------

If a time dependent stimulus was presented, the original stimulus file is
saved in the experimental folder.

Video
=====

After recording a :ref:`video<VideoLabel>`, you will
find a folder with the "DATE_TIME_EXP.GROUP" as its name. An example
would be "2019.01.11_14-00-05_CantonS". This is an experiment
conducted on the 11th of January 2019. "CantonS" is the value that
was entered in the field "Exp. Group".

"EXPGRP_VIDEO.h264"
-------------------
is the video file. This video file on its own is not perfectly useful
(at least in my hands) as h264 seems to be a bit of an exotic file
format that many video players can not handle without problems.

In order to directly convert this file, see the :ref:`Image Data
handling instructions <h264_to_avi_GUI>`. If you want to a
GUI-free version of these modules, check out the
"convert_h264_to_AVI.py" at
https://gitlab.com/davidtadres/pivr_bonus

.. note::
   I have tried to directly convert the image using ffmpeg. I believe
   there is a bug somewhere in the encoder of the camera as ffmpeg
   reads that the video is "inf" long. The scripts above take the
   video metadata from "experiment_settings.json" to properly convert
   the video.

The standard lens introduces a lot of radial aberrations at the
edges! To fix them see :ref:`undistort h264 video <undistort_h264_GUI>`.

.. figure:: Figures/ouput/undistortExample.png
  :width: 100 %
  :alt: undistortExample.png

To correct these distortions, just make sure you have selected
:ref:`Perform Undistort <UndistortOptionsLabel>` before running the
:ref:`video conversion <h264_to_avi_GUI>`.

"DATE_TIME_timestamps" OR "DATE_TIME_stimulation_presented.csv"
--------------------

If no time dependent stimulus has been presented, you will find a "DATE_TIME_timestamps.csv"
file in the folder containing:

   #. Frame (=image) number into the experiment
   #. Time in seconds since the experiment started

If a time dependent stimulus was presented, you will find a
"DATE_TIME_stimulation_presented.csv" file in the folder containing:

   #. Frame (=image) number into the experiment
   #. Time in seconds since the experiment started
   #. Channel 1 stimulus delivered
   #. Channel 2 stimulus delivered
   #. Channel 3 stimulus delivered
   #. Channel 4 stimulus delivered

"experiment_settings.json"
--------------------------
is a `json file <https://en.wikipedia.org/wiki/JSON>`_ and contains a
lot of useful experimental information:

#. Experiment Date and Time: Exactly as advertised

#. Framerate: The framerate the video was recorded in

#. Exp. Group: The string that was entered by the user during the
   experiment

#. Model Organism: If selected, what animal has been indicated
   during the experiment.

#. Pixel per mm: If defined (see :ref:`here<PixMMLabel>`) a useful
   parameter for analysis.

#. Recording time: The time in seconds that PiVR was recording
   this video.

#. Resolution: The camera resolution in pixels that PiVR used while
   recording the video.

#. Virtual Reality arena name: As no virtual arena was presented,
   it will say 'None'

#. backlight 2 channel: If Backlight 2 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 18)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. backlight channel: If Backlight 1 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 18)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].
   This would normally be defined as [18, 40000].

#. output channel 1: If Channel 1 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 17)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. output channel 2: If Channel 2 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 27)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. output channel 3: If Channel 3 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 13)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

#. output channel 4: If Channel 4 has been defined (as
   described :ref:`here<DefineGPIOsLabel>`) the chosen GPIO (e.g. 13)
   and the maximal PWM frequency (e.g. 40000) is saved as a [list].

(Optional) stimulation_file_used.csv
------------------------------------

If a time dependent stimulus was presented, the original stimulus file is
saved in the experimental folder.


Get started
===========

Different experiments necessitate different analysis. In the original
PiVR publication
`PiVR publication <https://doi.org/10.1371/journal.pbio.3000712>`__
a number of different experiments were run
and the analysis
`analysis <https://gitlab.com/LouisLab/pivr_publication>`__ and data
of those has been
made `public <https://doi.org/10.25349/D9ZK50>`__.
These scripts are all annotated and you
should be able to run them on your computer with the original data
to understand what is happening in them. Then you can adapt them/use
them with your data.

In addition, the PiVR software has a couple of built in analysis
tools when run on a PC (i.e. not on a Raspberry Pi):

.. _OutputPointExp:

Visualization of different points on the animal
===============================================

What exactly do the terms "Centroid" and "Midpoint" mean? I will try
to illustrate the difference so that you may choose the appropriate
parameter for your experiment:

#. To identify the animal the tracking algorithm identifies a "blob"
   that has significantly different pixel intensity values compared
   to the background.

#. The centroid is the center of mass (in 2D) of these pixels.

#. The midpoint is the center of the skeletonized blob.

   .. figure:: Figures/ouput/CM_illustration.png
      :width: 100 %
      :alt: CM_illustration.png