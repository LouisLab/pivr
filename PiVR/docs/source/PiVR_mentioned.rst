PiVR has been developed by David Tadres and Matthieu Louis (`Louis Lab
<https://labs.mcdb.ucsb.edu/louis/matthieu/>`__).

Community Building
******************

.. _Labs_using_PiVR:

Labs using PiVR
================

- Carrillo Lab (https://www.carrillolab.com/), University of Chicago, United States

- Clark Lab (https://matthewqclark.weebly.com/), Bucknell University, United States

- Louis Lab (https://louis.mcdb.ucsb.edu/), University of California, Santa Barbara, United States

- Schilder Lab (http://www.personal.psu.edu/rjs360/index.html), Pennsylvania State University, United States

- Sprecher Lab (http://www.sprecherlab.com), University of Fribourg, Switzerland

- Venton Lab (https://uva.theopenscholar.com/venton-group) , University of Virginia, USA


.. Note::

    If you are using PiVR, please :ref:`contact us <contact>` so that we
    can list you here.

.. _PiVR_for_teaching:

PiVR used for teaching
======================

- Drosophila Neurobiology: Genes, Circuits & Behavior (https://meetings.cshl.edu/courses.aspx?course=C-dros&year=22), Cold Spring Harbor, United States

- NEETO (https://voices.uchicago.edu/neeto/gallery/), University of Chicago, United States

- Pueblo Brain Science (https://www.neuraldiversity.org/nsf-career), University of New Mexico, United States

- Santa Barbara Advanced School Of Quantitative Biology (https://www.kitp.ucsb.edu/qbio), University of California, Santa Barbara, United States

.. _PiVR_in_press:

PiVR in the press
==================

- `Scientific American <https://www.scientificamerican.com/article/fruit-flies-plug-into-the-matrix/>`__

- `Eureka Alert <https://www.eurekalert.org/pub_releases/2020-07/p-arp070720.php>`__

- `Laboratory News <https://www.labnews.co.uk/article/2030709/serving-virtual-reality-raspberry-pi-to-flies>`__

- `Nano Werk <https://www.nanowerk.com/news2/biotech/newsid=55649.php>`__

- `Science Daily <https://www.sciencedaily.com/releases/2020/07/200714143044.htm>`__

- `SciTechDaily <https://scitechdaily.com/virtual-reality-system-for-small-animals-based-on-raspberry-pi/>`__

- `UCSB News <https://www.news.ucsb.edu/2020/019955/affordable-alternative>`__

PiVR on blogs
==============

- `LabOnTheCheap.com <https://www.labonthecheap.com/pivr-415-to-create-a-vr-platform-for-small-creatures/>`__

- `Open-Neuroscience.com <https://open-neuroscience.com/post/pivr/>`__

- `OpenBehavior <https://edspace.american.edu/openbehavior/project/pivr/>`__
