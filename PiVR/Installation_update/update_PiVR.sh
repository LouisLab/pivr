#!/bin/bash

# IMPORTANT: Don't open this file on Windows and save again. The
# reason being that line endings are different between DOS/Windows
# and Unix/Linux. DOS uses carriage return and line feed ('\r\n')
# while Unix uses just line feed ('\n').
# If this file is opened and saved in Windows just run this command
# before opening the sh file on Linux
# tr -d '\r' <update_PiVR.sh> outfile
# then rename the 'outfile' and delete the original

# check if gitlab.com can be reached, in effect checking if the RPi is 
# connected to the internet!
wget -q --spider https://gitlab.com

if [ $? -eq 0 ]; then

	echo "Gitlab.com can be reached...updating now!"
	# update all the software on the RPi
	yes Y | sudo apt-get update

	# change directory
	cd ~/PiVR
	# pull newest changes from master branch
	git pull

	# done
	echo "Done updating!"
else
	{ echo "Gitlab.com could not be reached!"; exit $ERRCODE 1; }
	
	#echo "Offline"
fi




