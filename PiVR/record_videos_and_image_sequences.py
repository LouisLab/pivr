__author__ = 'David Tadres'
__project__ = 'PiVR'

import datetime
import json
import os
import time
import tkinter as tk
from pathlib import Path

import numpy as np
import pandas as pd

# this try-except statement checks if the processor is a ARM processor
# (used by the Raspberry Pi) or not.
# Since this command only works in Linux it is caught using
# try-except otherwise it's throw an error in a Windows system.
try:
    if os.uname()[4][:3] == 'arm':
        # This will yield True for both a Raspberry and for M1 Chip
        # Apple devices.
        # Use this code snippet
        # (from https://raspberrypi.stackexchange.com/questions/5100/detect-that-a-python-program-is-running-on-the-pi)
        import re
        CPUINFO_PATH = Path("/proc/cpuinfo")
        if CPUINFO_PATH.exists():
            with open(CPUINFO_PATH) as f:
                cpuinfo = f.read()
            if re.search(r"^Model\s*:\s*Raspberry Pi", cpuinfo, flags=re.M) is not None:
                # if True, is Raspberry Pi
                RASPBERRY = True
                LINUX = True
        else: # Test if one more intendation necessary or not. On Varun's computer
            # is Apple M1 chip (or other Arm CPU device).
            RASPBERRY = False
            LINUX = True
    else:
        # is either Mac or Linux
        RASPBERRY = False
        LINUX = True

    DIRECTORY_INDICATOR = '/'
except AttributeError:
    # is Windows
    RASPBERRY = False
    LINUX = False
    DIRECTORY_INDICATOR = '\\'

if RASPBERRY:
    pass


class RecordVideo():
    def __init__(self,
                 genotype='Unknown',
                 recording_framerate=2,
                 display_framerate = None,
                 resolution=[640, 480],
                 recordingtime=None,
                 signal=None,
                 cam = None,
                 base_path=None,
                 time_dependent_stim_file=None,
                 #stim_GPIO=None,
                 pwm_object=None,
                 model_organism=None,
                 pixel_per_mm=None,
                 output_channel_one=[],
                 output_channel_two=[],
                 output_channel_three=[],
                 output_channel_four=[],
                 high_power_LED_bool=False,
                 pwm_range=None,
                 background_channel=[],
                 background_2_channel=[],
                 background_dutycycle=[],
                 background_2_dutycycle=[],
                 version_info = None
                 ):
        """
        changed this by (mis?)-using the motion detector class that
        comes with the picamera module.
        It enables me to do stuff after capturing each frame while
        compressing the video to h264 format (seems to be quite
        lossy, but test!) Good thing is that with this script we are
        able to go to almost 90fps while recording a video at 640*480
        and also controlling the LEDs and other, none CPU intensive
        stuff.
       """

        self.genotype = genotype
        self.recording_framerate = recording_framerate
        self.resolution = resolution
        self.recording_time = recordingtime
        self.cam = cam
        self.path = base_path
        self.time_dependent_stim_file = time_dependent_stim_file
        #self.stim_GPIO=stim_GPIO
        self.pwm_object = pwm_object
        self.output_channel_one = output_channel_one
        self.output_channel_two = output_channel_two
        self.output_channel_three = output_channel_three
        self.output_channel_four = output_channel_four
        self.high_power_LED_bool = high_power_LED_bool
        self.pwm_range = pwm_range
        self.background_channel = background_channel
        self.background_2_channel = background_2_channel
        self.background_dutycycle = background_dutycycle
        self.background_2_dutycycle = background_2_dutycycle
        self.version_info = version_info

        self.datetime = time.strftime("%Y.%m.%d_%H-%M-%S")

        os.makedirs(self.path + '/' + self.datetime + '_' + self.genotype, exist_ok=True)  # make the directory
        os.chdir(self.path + '/' + self.datetime + '_' + self.genotype)

        if self.high_power_LED_bool:
            PiVR_LED_version = 'High Powered LED version'
        else:
            PiVR_LED_version = 'Standard LED version'

        experiment_info = {'PiVR info (recording)': self.version_info,
                           'Camera Shutter Speed [us]': self.cam.exposure_speed,
                           'Experiment Date and Time': self.datetime,
                           'Framerate': recording_framerate,
                           'Pixel per mm': pixel_per_mm,
                           'Exp. Group': genotype,
                           'Resolution': resolution,
                           'Recording time': recordingtime,
                           'Model Organism': model_organism,
                           'output channel 1' : self.output_channel_one,
                           'output channel 2' : self.output_channel_two,
                           'output channel 3' : self.output_channel_three,
                           'output channel 4' : self.output_channel_four,
                           'backlight channel' : self.background_channel,
                           'backlight 2 channel' : self.background_2_channel,
                           'backlight dutycycle' : self.background_dutycycle,
                           'backlight 2 dutcycle' : self.background_2_dutycycle,
                           'PiVR LED version' : PiVR_LED_version
                           }
        with open((self.path + '/' + self.datetime + '_' + self.genotype + '/' + 'experiment_settings.json'), 'w') as file:
            json.dump(experiment_info, file, sort_keys=True, indent=4)

        if self.time_dependent_stim_file is not None:
            self.time_dependent_stim_file.to_csv(self.path + '/' + self.datetime + '_' +
                                                 self.genotype + '/stimulation_file_used.csv')

            # When the high powered PiVR version is used, the software has
            # to handle the unfortunate fact that the LED controller of the
            # high powered PiVR version is completely ON when the GPIO is
            # OFF and vice versa. This of course is the opposite of what
            # happens in the normal version.
            #
            # Internally, the software must therefore invert the stimulus if
            # that's the case. This function takes care of this.
            #
            # The end user does not need to know this. From their
            # perspective they are able to use the same input arena they
            # would use for the standard version while getting the expected
            # result.
            if self.high_power_LED_bool:
                for current_column in self.time_dependent_stim_file.columns:
                    if 'Channel' in current_column:
                        self.time_dependent_stim_file[current_column] = \
                            self.pwm_range - self.time_dependent_stim_file[current_column]


        self.recording_video_w_output()

    def recording_video_w_output(self):

        '''
        After playing around with other option, using the h264
        encoder directly on the GPU is by far the fastest option.
        At 640x480 it can sustain over 85fps for a very long time -
        especially if only a small part of the image is changing over
        time (which would be the case in many scientific tracking
        situations). Using the motion_output argument that comes with
        the picamera functions I was able to sneak in the pwm_object.
        This means that we can not only record at very high speed
        but that we can also turn on the LED to any intensity at the
        same speed as we are recording the video
        :return:
        '''

        print('recording video at ' + repr(self.recording_framerate) + ' fps for at total time of ' +
              repr(self.recording_time))

        # set the framerate (shouldn't be necessary)
        self.cam.framerate = self.recording_framerate
        # start counting time
        start = time.time()
        # start the video recording. The motion output parameter is the easiest way I found to 'do something'
        # after each frame has been taken. Specifically time is recorded and if there's a stimulation, the GPIO's
        # are turned on or off.
        self.cam.start_recording(self.genotype + '_Video.h264', format='h264',
                                 motion_output=ModifyFrameVideo(
                                 pwm_object= self.pwm_object,
                                 recording_time=self.recording_time,
                                 recording_framerate=self.recording_framerate,
                                 time_dependent_stim_file=self.time_dependent_stim_file,
                                 output_channel_one=self.output_channel_one,
                                 output_channel_two=self.output_channel_two,
                                 output_channel_three=self.output_channel_three,
                                 output_channel_four=self.output_channel_four,
                                 datetime=self.datetime,
                                 high_power_LED_bool=self.high_power_LED_bool,
                                 pwm_range=self.pwm_range,
                                 cam=self.cam)
                                 )
        self.cam.wait_recording(self.recording_time)
        self.cam.stop_recording()
        finish = time.time()
        print('Captured '+repr(self.recording_framerate*self.recording_time)+ ' images at %.2ffps' %
              (self.recording_framerate*self.recording_time / (finish - start)))

    '''
    def record_video(self):
        """
        A placeholder function which can be used to in case the other function breaks. This will only record a video
        and should work out of the box. Decreased functionality, i.e. no light control possible.
        :return:
        """
        # make sure the framerate is as expected
        self.cam.framerate = self.recording_framerate
        # start recording
        self.cam.start_recording(repr(self.genotype) + 'Video.h264', format='h264')
        # Next block everything so that the recording can get along. Prefereable to time.sleep because:
        # It is recommended that this method is called while recording to check for exceptions.
        # http://picamera.readthedocs.io/en/release-1.10/api_camera.html
        self.cam.wait_recording(self.recording_time)
        # After the video has been recorded, stop the recording
        self.cam.stop_recording() 
    '''

class ModifyFrameVideo(object):
    """
    This class is called after each frame is taken. It saves the
    current time since the start of the recording.
    If the user defined a stimulation protocol it will also update
    GPIO's voltage accordingly.
    """
    def __init__(self,
                 pwm_object = None,
                 recording_time=None,
                 recording_framerate=None,
                 time_dependent_stim_file=None,
                 #stim_GPIO=None,
                 output_channel_one=[],
                 output_channel_two=[],
                 output_channel_three=[],
                 output_channel_four=[],
                 datetime='Not defined',
                 high_power_LED_bool=False,
                 pwm_range=None,
                 cam=None
                 ):

        self.size = 0
        self.counter = 0
        self.pwm_object = pwm_object
        self.recording_time=recording_time
        self.recording_framerate=recording_framerate
        self.start_time = False
        self.time_dependent_stim_file = time_dependent_stim_file
        #self.stim_GPIO = stim_GPIO
        self.previous_channel_one_value = None
        self.previous_channel_two_value = None
        self.previous_channel_three_value = None
        self.previous_channel_four_value = None
        self.output_channel_one = output_channel_one
        self.output_channel_two = output_channel_two
        self.output_channel_three = output_channel_three
        self.output_channel_four = output_channel_four
        self.datetime = datetime
        self.high_power_LED_bool = high_power_LED_bool
        self.pwm_range=pwm_range
        self.cam=cam

        # Pre-allocate empty array to save timepoint of each recorded frame
        #self.real_time = np.zeros((int(self.recording_framerate*self.recording_time)))
        self.real_time = []
        #self.stimulation = np.zeros((4, int(self.recording_framerate*self.recording_time)))
        self.stimulation_ch1 = []
        self.stimulation_ch2 = []
        self.stimulation_ch3 = []
        self.stimulation_ch4 = []

    def update_pwm_dutycycle_time_dependent(self,
                                            previous_channel_value,
                                            output_channel_list,
                                            output_channel_name):
        """
        A convenience function for the time dependent stimulation.
        Takes the list with the gpios for a given channel,
        and, in a for loop, updates gpios according to a given channel.
        In the first iteration of the loop it will just set the PWM
        dutcycle according to whatever dutycycle is specified.
        As this function is called as
        'previous_channel_x_value = update_pwm_dutcycle..' it then
        updates the previous_channel_x_value for the next iteration.
        :param previous_channel_value: As the GPIO dutcycle should
        only be updated when the value changes, this holds the
        previous value
        :param output_channel_list: list of gpio for a given channel,
        e.g. GPIO 17 would be [[17,1250]] (1250 is the frequency,
        not used here)
        :param output_channel_name: the channel as a string,
        e.g. 'Channel 1'
        """

        # This is new and adresses #96: instead of time dependent stim
        # going only for index, we check the time since the experiment
        # started to find the stim intensity to present.
        # note - self.real_time comes in us. Need to convert to seconds.
        current_time = self.real_time[-1] / 1e6
        # np.searchsorted takes care of the fact that the current
        # time is never going to
        # be exactly the same as any number here
        if 'Time [s]' in self.time_dependent_stim_file:
            stim_index = np.searchsorted(
                self.time_dependent_stim_file['Time [s]'], current_time)
        else:
            # for backward compatibility keep the original way of
            # presenting the time dependent stimulus
            stim_index = self.counter

        if self.counter == 0 or \
            previous_channel_value != self.time_dependent_stim_file[output_channel_name][stim_index]:

            for i_stim in range(len(output_channel_list)):
                #if self.high_power_LED_bool:
                #
                #    self.pwm_object.set_PWM_dutycycle(
                #        user_gpio=output_channel_list[i_stim][0],
                #        dutycycle=self.pwm_range\
                #                  - self.time_dependent_stim_file[
                #                      output_channel_name][stim_index])
                #else:
                self.pwm_object.set_PWM_dutycycle(
                    user_gpio=output_channel_list[i_stim][0],
                    dutycycle=self.time_dependent_stim_file[output_channel_name][stim_index])

        return(self.time_dependent_stim_file[output_channel_name][stim_index])

    def write(self, s):
        """
        This function is called every time the Pi camera returns a
        frame. It currently saves the time in the real_time array
        which will finally be saved as 'timestamps.csv'
        If a time dependent array is provided it will also update
        the GPIO's accordingly. Same function as in the tracking is
        called.
        :param s: placeholder
        :return:
        """

        print('Frame #: ' + repr(self.counter))
        # It seems to happenx that there are more frames than expected sometimes
        # To avoid index error (see #96), first test if the counter is
        # larger than the expected frame number.
        #if self.counter < self.real_time.shape[0]:
        #print('start_time ' + repr(self.start_time))
        #print("self.cam.timestamp " + repr(self.cam.timestamp))
        if self.cam.timestamp:
            if not self.start_time:
                self.start_time = self.cam.timestamp
                self.real_time.append(0)
            else:
                self.real_time.append(self.cam.timestamp - self.start_time)

        if self.time_dependent_stim_file is not None:
            try:
                self.previous_channel_one_value = \
                    self.update_pwm_dutycycle_time_dependent(
                        previous_channel_value=self.previous_channel_one_value,
                        output_channel_list=self.output_channel_one,
                        output_channel_name='Channel 1')
            except KeyError:
                pass

            try:
                self.previous_channel_two_value = \
                    self.update_pwm_dutycycle_time_dependent(
                        previous_channel_value=self.previous_channel_two_value,
                        output_channel_list=self.output_channel_two,
                        output_channel_name='Channel 2')
            except KeyError:
                pass

            try:
                self.previous_channel_three_value = \
                    self.update_pwm_dutycycle_time_dependent(
                        previous_channel_value=self.previous_channel_three_value,
                        output_channel_list=self.output_channel_three,
                        output_channel_name='Channel 3')
            except KeyError:
                pass

            try:
                self.previous_channel_four_value = \
                    self.update_pwm_dutycycle_time_dependent(
                        previous_channel_value=self.previous_channel_four_value,
                        output_channel_list=self.output_channel_four,
                        output_channel_name='Channel 4')
            except KeyError:
                pass

            try:
                if len(self.output_channel_one) > 0:
                    # save the stimulation delivered to the pwm.object
                    #self.stimulation[0, self.counter] = self.previous_channel_one_value
                    self.stimulation_ch1.append(self.previous_channel_one_value),
                else:
                    self.stimulation_ch1.append(0)
                if len(self.output_channel_two) > 0:
                    #self.stimulation[1, self.counter] = self.previous_channel_two_value
                    self.stimulation_ch2.append(self.previous_channel_two_value)
                else:
                    self.stimulation_ch2.append(0)
                if len(self.output_channel_three) > 0:
                    #self.stimulation[2, self.counter] = self.previous_channel_three_value
                    self.stimulation_ch3.append(self.previous_channel_three_value)
                else:
                    self.stimulation_ch3.append(0)
                if len(self.output_channel_four) > 0:
                    #self.stimulation[3, self.counter] = self.previous_channel_four_value
                    self.stimulation_ch4.append(self.previous_channel_four_value)
                else:
                    self.stimulation_ch4.append(0)
            except KeyError:
                # This could happen when the stimulation file is shorter than the time of the recording
                pass
        #else:
        #    print('Counter is higher than expected frames ' + repr(self.counter))
        self.counter +=1

    def flush(self):
        print(len(self.stimulation_ch1))
        if self.time_dependent_stim_file is not None:
            csv_object = pd.DataFrame({'Frame number': np.arange(0, len(self.real_time)),
                                       'Time [s]': (np.array(self.real_time) - self.real_time[0]) / 1e6,
                                       'Channel 1': np.array(self.stimulation_ch1),
                                       'Channel 2': np.array(self.stimulation_ch2),
                                       'Channel 3': np.array(self.stimulation_ch3),
                                       'Channel 4': np.array(self.stimulation_ch4)})

            csv_object.to_csv(self.datetime + '_stimulation_presented.csv', sep=',')
        else:

            csv_object = pd.DataFrame({'Frame number': np.arange(0, len(self.real_time)),
                                       'Time [s]': (np.array(self.real_time) - np.array(self.real_time[0])) / 1e6})

            csv_object.to_csv(self.datetime + '_timestamps.csv', sep=',')

class RecordFullFrameImages():
    """
    This class lets the user record images
    """
    def __init__(self,
                 cam = None,
                 base_path = None,
                 genotype = None,
                 recording_framerate = 2,
                 resolution = [640,480],
                 recording_time = 0,
                 pixel_per_mm = None,
                 model_organism = 'Not in list',
                 time_dependent_stim_file = None,
                 #stim_GPIO=None,
                 pwm_object = None,
                 output_channel_one=[],
                 output_channel_two=[],
                 output_channel_three=[],
                 output_channel_four=[],
                 image_format='jpg',
                 high_power_LED_bool = 0,
                 pwm_range = 0,
                 background_channel=[],
                 background_2_channel=[],
                 background_dutycycle=[],
                 background_2_dutycycle=[],
                 version_info = None
                 ):

        '''
        :param cam:
        :param base_path:
        :param genotype:
        :param recording_framerate:
        :param resolution:
        :param recording_time:
        :param pixel_per_mm:
        :param model_organism:
        '''

        self.cam = cam
        self.path = base_path
        self.genotype = genotype
        self.recording_framerate = recording_framerate
        self.resolution = resolution
        self.recording_time = recording_time
        self.pixel_per_mm = pixel_per_mm
        self.model_organism = model_organism
        self.time_dependent_stim_file = time_dependent_stim_file
        #self.stim_GPIO = stim_GPIO
        self.pwm_object = pwm_object
        self.trailing_zeros = None
        self.previous_channel_one_value = None
        self.previous_channel_two_value = None
        self.previous_channel_three_value = None
        self.previous_channel_four_value = None
        self.output_channel_one = output_channel_one
        self.output_channel_two = output_channel_two
        self.output_channel_three = output_channel_three
        self.output_channel_four = output_channel_four
        self.image_format = image_format
        self.real_time = None
        self.start_time = False
        self.stimulation = None
        self.high_power_LED_bool = high_power_LED_bool
        self.pwm_range = pwm_range
        self.background_channel = background_channel
        self.background_2_channel = background_2_channel
        self.background_dutycycle = background_dutycycle
        self.background_2_dutycycle = background_2_dutycycle
        self.version_info = version_info
        self.counter = 0

        self.record_image_sequence_func()

    def update_pwm_dutycycle_time_dependent(self,
                                            previous_channel_value,
                                            output_channel_list,
                                            output_channel_name):
        """
        A convenience function for the time dependent stimulation.
        Takes the list with the gpios for a given channel, and,
        in a for loop, updates gpios according to a given channel.
        In the first iteration of the loop it will just set the PWM
        dutcycle according to whatever dutycycle is specified.
        As this function is called as
        'previous_channel_x_value = update_pwm_dutcycle..' it then
        updates the previous_channel_x_value for the next iteration.
        :param previous_channel_value: As the GPIO dutycycle should
        only be updated when the value changes, this holds the
        previous value
        :param output_channel_list: list of gpio for a given channel,
        e.g. GPIO 17 would be [[17,1250]] (1250 is the frequency,
        not used here)
        :param output_channel_name: the channel as a string,
        e.g. 'Channel 1'
        """

        # This is new and adresses #96: instead of time dependent stim
        # going only for index, we check the time since the experiment
        # started to find the stim intensity to present.
        # note - self.real_time comes in us. Need to convert to seconds.
        current_time = self.real_time[self.counter] / 1e6
        # np.searchsorted takes care of the fact that the current
        # time is never going to
        # be exactly the same as any number here
        if 'Time [s]' in self.time_dependent_stim_file:
            stim_index = np.searchsorted(
                self.time_dependent_stim_file['Time [s]'], current_time)
        else:
            # for backward compatibility keep the original way of
            # presenting the time dependent stimulus
            stim_index = self.counter

        #print('Frame#: ' + repr(self.counter))
        #print('Time: ' + repr(current_time))
        #print('Stim index: ' + repr(stim_index))
        #print('Stim: ' + repr(self.time_dependent_stim_file[output_channel_name][stim_index]))

        if self.counter == 0 or \
            previous_channel_value != self.time_dependent_stim_file[output_channel_name][stim_index]:

            for i_stim in range(len(output_channel_list)):
                #if self.high_power_LED_bool:
                #    self.pwm_object.set_PWM_dutycycle(user_gpio=output_channel_list[i_stim][0],
                #                                      dutycycle=self.pwm_range -
                #                                                self.time_dependent_stim_file[output_channel_name][
                #                                                    stim_index])
                #else:
                self.pwm_object.set_PWM_dutycycle(user_gpio=output_channel_list[i_stim][0],
                                                  dutycycle=self.time_dependent_stim_file[output_channel_name][stim_index])
        return(self.time_dependent_stim_file[output_channel_name][stim_index])

    def record_image_sequence_func(self):
        datetime = time.strftime("%Y.%m.%d_%H-%M-%S")


        if RASPBERRY:
            os.makedirs(self.path + '/' + datetime + '_' + self.genotype, exist_ok=True)  # make the directory
            os.chdir(self.path + '/' + datetime + '_' + self.genotype)

        if self.high_power_LED_bool:
            PiVR_LED_version = 'High Powered LED version'
        else:
            PiVR_LED_version = 'Standard LED version'
        experiment_info = {'PiVR info (recording)': self.version_info,
                           'Camera Shutter Speed [us]': self.cam.exposure_speed,
                           'Experiment Date and Time' : datetime,
                           'Exp. Group': self.genotype,
                           'Framerate': self.recording_framerate,
                           'Resolution': self.resolution,
                           'Recording time': self.recording_time,
                           'Pixel per mm': self.pixel_per_mm,
                           'Model Organism': self.model_organism,
                           'output channel 1' : self.output_channel_one,
                           'output channel 2' : self.output_channel_two,
                           'output channel 3' : self.output_channel_three,
                           'output channel 4' : self.output_channel_four,
                           'backlight channel' : self.background_channel,
                           'backlight 2 channel' : self.background_2_channel,
                           'backlight dutycycle' : self.background_dutycycle,
                           'backlight 2 dutcycle' : self.background_2_dutycycle,
                           'PiVR LED version': PiVR_LED_version,
                           }

        # to make it easier to convert images to videos keep filename
        # length, identical. Eg. if we have a total of 25 images it's
        # ok to do image01..image02...image25. But if the total
        # number of images is 1500 the length changes. Better to then
        # use image0001..image1500
        self.trailing_zeros = len(str(self.recording_time*self.recording_framerate))

        with open((self.path + '/' + datetime + '_' + self.genotype +
                   '/' + 'experiment_settings.json'), 'w') as file:
            json.dump(experiment_info, file, sort_keys=True, indent=4)

        if self.time_dependent_stim_file is not None:
            self.time_dependent_stim_file.to_csv(self.path + '/' + datetime + '_' +
                                                 self.genotype + '/stimulation_file_used.csv')

        #else:
        self.real_time = np.zeros((int(self.recording_time*self.recording_framerate)))
        self.stimulation = np.zeros((4, self.recording_time*self.recording_framerate))

        start_time = time.time()
        self.cam.capture_sequence(self.record_sequence_func(),
                                  use_video_port=True)
        finish = time.time()


        csv_object = pd.DataFrame({'Frame number' : np.arange(0,self.real_time.shape[0]),
                                    'Time [s]' : self.real_time/1e6,
                                   'Channel 1' : self.stimulation[0,:],
                                   'Channel 2' : self.stimulation[1,:],
                                   'Channel 3' : self.stimulation[2,:],
                                   'Channel 4' : self.stimulation[3,:]
                                   })

        csv_object.to_csv(datetime + '_stimulation_presented.csv',sep=',')

        print('Captured %d frames at %.2ffps' % (int(self.recording_framerate*self.recording_time),
                                                 int(self.recording_framerate * self.recording_time) /
                                                 (finish - start_time)))

    def record_sequence_func(self):
        """
        generator function that saves time a frame was taken, adjusts
        GPIO voltage according to a time dependent stimulus file and
        yield images.
        For easier postprocessing the leading zeros of the image (
        e.g. image0001.jpg) is adjusted so that the length
        of the name does not change for one folder.
        """
        while self.counter < int(self.recording_time * self.recording_framerate):
            #timestamps[frame, 1] = time.time()
            #self.real_time['Time'][frame] = time.time() - self.start_time
            if not self.start_time:
                self.start_time = self.cam.timestamp
            try:
                self.real_time[self.counter] = self.cam.timestamp \
                                                  - self.start_time
            except IndexError:
                # if too many frames - TODO make better
                pass

            #print('real time: ' + repr(self.real_time[self.counter]))
            #self.real_time[self.counter] = time.time()

            print('Frame#: ' + repr(self.counter))

            if self.time_dependent_stim_file is not None:
                try:
                    self.previous_channel_one_value = \
                        self.update_pwm_dutycycle_time_dependent(
                            previous_channel_value=self.previous_channel_one_value,
                            output_channel_list=self.output_channel_one,
                            output_channel_name='Channel 1')
                except KeyError:
                    pass

                try:
                    self.previous_channel_two_value = \
                        self.update_pwm_dutycycle_time_dependent(
                            previous_channel_value=self.previous_channel_two_value,
                            output_channel_list=self.output_channel_two,
                            output_channel_name='Channel 2')
                except KeyError:
                    pass

                try:
                    self.previous_channel_three_value = \
                        self.update_pwm_dutycycle_time_dependent(
                            previous_channel_value=self.previous_channel_three_value,
                            output_channel_list=self.output_channel_three,
                            output_channel_name='Channel 3')
                except KeyError:
                    pass

                try:
                    self.previous_channel_four_value = \
                        self.update_pwm_dutycycle_time_dependent(
                            previous_channel_value=self.previous_channel_four_value,
                            output_channel_list=self.output_channel_four,
                         output_channel_name='Channel 4')
                except KeyError:
                    pass

                if len(self.output_channel_one) > 0:
                    self.stimulation[0, self.counter] = self.previous_channel_one_value
                if len(self.output_channel_two) > 0:
                    self.stimulation[1, self.counter] = self.previous_channel_two_value #self.time_dependent_stim_file['Channel 2'][self.counter]
                if len(self.output_channel_three) > 0:
                    self.stimulation[2, self.counter] = self.previous_channel_three_value
                if len(self.output_channel_four) > 0:
                    self.stimulation[3, self.counter] = self.previous_channel_four_value


            yield str('image%0' + repr(self.trailing_zeros) + 'd.' + self.image_format) % self.counter

            self.counter += 1

class Timelapse():
    """
    This class lets the user record full frame images for long times
    for framerates below 2fps (the lower limit for other recording
    modes)
    """

    def __init__(self,
                 cam = None,
                 base_path = None,
                 genotype = None,
                 resolution = [640,480],
                 recordingtime = 0,
                 time_between = 0,
                 pixel_per_mm = None,
                 model_organism = 'Not in list',
                 image_format = 'jpg',
                 pwm_range = 0,
                 background_channel = [],
                 background_2_channel = [],
                 background_dutycycle = [],
                 background_2_dutycycle = [],
                 version_info=None):

        self.genotype = genotype
        self.resolution = resolution
        self.total_recording_time = recordingtime
        self.time_between_captures = time_between
        self.pixel_per_mm = pixel_per_mm
        self.model_organism = model_organism
        self.cam = cam
        self.path = base_path
        self.image_format = image_format
        self.pwm_range = pwm_range
        self.background_channel = background_channel
        self.background_2_channel = background_2_channel
        self.background_dutycycle = background_dutycycle
        self.background_2_dutycycle = background_2_dutycycle
        self.version_info = version_info

        datetime = time.strftime("%Y.%m.%d_%H-%M-%S")

        if RASPBERRY:
            self.current_path = Path(self.path, datetime + '_' + self.genotype)
            self.current_path.mkdir(parents=True, exist_ok=True)
            #os.makedirs(self.path + '/' + datetime + '_' + self.genotype, exist_ok=True)  # make the directory
            #os.chdir(self.path + '/' + datetime + '_' + self.genotype)

        experiment_info = {'PiVR info (recording)': self.version_info,
                           'Camera Shutter Speed [us]': self.cam.exposure_speed,
                           'Experiment Date and Time' : datetime,
                           'Exp. Group': self.genotype,
                           'Resolution': self.resolution,
                           'Total Recording Time[s]': self.total_recording_time,
                           'Time between Images[s]': self.time_between_captures,
                           'Pixel per mm': self.pixel_per_mm,
                           'Model Organism': self.model_organism,
                           'backlight channel' : self.background_channel,
                           'backlight 2 channel' : self.background_2_channel,
                           'backlight dutycycle' : self.background_dutycycle,
                           'backlight 2 dutcycle' : self.background_2_dutycycle,
                           }
        with open((self.path + '/' + datetime + '_' + self.genotype +
                   '/' + 'experiment_settings.json'), 'w') as file:
            json.dump(experiment_info, file, sort_keys=True, indent=4)

        self.child = tk.Toplevel()
        self.child.grab_set()
        # set a title for the window
        self.child.wm_title('Stop Window')
        self.stop_button = tk.Button(self.child,
                                     text='STOP TIMELAPSE',
                                     font="Helvetica 10 bold",
                                     command=self.stop_capture)
        self.stop_button.grid(row=0,column=0)
        self.stop_button.update()

        self.do_capture = True

        self.i_frame = 0

        self.global_start_time = time.time()

        self.control_time_func()

    def control_time_func(self):
        if self.do_capture:
            # collect start time of the loop
            start_time = time.time()
            # Grab the frame
            self.grab_image_func()
            # keep track of number of recorded frames
            self.i_frame += 1
            # if desired time is reached, stop the aquisition
            if time.time() - self.global_start_time >= \
                    self.total_recording_time:
                self.do_capture = False
                print('Timelapse has finished as expected.')
                # Once the while loop has been broken set the main window
                # active again
                self.child.grab_release()
                # close the tkinter window
                self.child.destroy()
            else:
                # if more images need to be collected, get end of
                # loop time
                end_time = time.time()

                time_lost_while_capturing = end_time - start_time
                self.child.after(int(round((self.time_between_captures -
                                  time_lost_while_capturing) * 1000)),
                              self.control_time_func)
        else:
            # Once the while loop has been broken set the main window
            # active again
            self.child.grab_release()
            # close the tkinter window
            self.child.destroy()

    def grab_image_func(self):
        current_time = datetime.datetime.now()
        image_name = current_time.strftime("%Y_%m_%d-%H_%M_%S")\
                           + '.' + self.image_format
        self.cam.capture(str(Path(self.current_path, image_name)))
        print('Frame #' + repr(self.i_frame) + ' captured.')

    def stop_capture(self):
        self.do_capture = False

        # Unsure if necessary to update the experiment setting
        # file...Info is in the images themselves since they are
        # timestamped.

        # Once the while loop has been broken set the main window
        # active again
        self.child.grab_release()
        # close the tkinter window
        self.child.destroy()


