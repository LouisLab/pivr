__author__ = 'David Tadres'
__project__ = 'PiVR'

import operator
import time
import tkinter as tk
from glob import glob
from tkinter import filedialog
from tkinter import messagebox

import matplotlib.animation as animation
import matplotlib.backends.backend_tkagg as tkagg
import numpy as np
import pandas as pd
from matplotlib.figure import Figure
from scipy import ndimage
from skimage import draw

try:
    import cv2
    CV2_INSTALLED = True
except ModuleNotFoundError:
    CV2_INSTALLED = False

import tracking_help_classes as thc

# turn of pandas copy warning
pd.options.mode.chained_assignment = None  # default='warn'

class VisualizeTrackedExperiment():
    """
    When the online tracking method has been used, either just
    tracking or to present a virtual reality, it can be
    interesting to see what happened during the experiment as if
    watching a video.

    This is non-trivial as the images has been deconstructed to
    "background" and "animal" during the tracking process.

    This class puts the "animal" and "background" back together and
    displays the result in something like a video player.

    The way this class is currently written is quite CPU heavy: Every
    time the figure is updated, the image to be shown is
    reconstructed (as opposed to a strategy where this process is
    done once at the beginning). I decided to go this way for maximal
    flexibility, but this can be changed in the future, of course.

    As usual, the :func:`VisualizeTrackedExperiment.__init__` creates
    the GUI the user interacts with.

    TODO: continue here
    """
    def __init__(self,
                 csv_data,
                 background_image,
                 data_path,
                 recording_framerate,
                 colormap,
                 signal,
                 undistort_data=None):

        self.csv_data = csv_data
        self.background_image = background_image
        self.data_path = data_path
        self.recording_framerate = recording_framerate
        self.colormap = colormap
        self.undistort_data = undistort_data

        if undistort_data is not None:
            height, width = background_image.shape

            self.newcameramtx, roi = \
                cv2.getOptimalNewCameraMatrix(
                    self.undistort_data['mtx'],
                    self.undistort_data['dist'],
                    (width, height), 1, (width, height))

            self.map1, self.map2 = cv2.initUndistortRectifyMap(
                cameraMatrix=self.undistort_data['mtx'],
                distCoeffs=self.undistort_data['dist'],
                R=None,# I think this is rotation
                newCameraMatrix=self.newcameramtx,
                size=(width, height),
                m1type=cv2.CV_8U # 8bit, 1-byte unsigned integer, e.g. CV_32F would be 32bit/4-byte floating point
                                                     )

        self.image_number = tk.IntVar()
        self.image_number.set(0)
        self.initalize = True
        self.callback_func_variable = 0
        self.playback_speed_options = ('0.1X', '0.5X', '1X',
                                       '2X', '5X', 'Custom')
        self.playback_speed_variable = tk.StringVar()
        self.playback_speed_variable.set(self.playback_speed_options[2])
        self.playback_speed_value = 1.0
        self.update_overview_var = tk.StringVar()
        self.update_raw_var = tk.StringVar()
        self.update_binary_raw = tk.StringVar()
        self.update_overview_bool = True
        self.plot_centroids = True
        if 'X-Head' in self.csv_data.columns:
            self.plot_heads = True
        else:
            self.plot_heads = False
        if 'X-Tail' in self.csv_data.columns:
            self.plot_tails = True
        else:
            self.plot_tails = False

        self.vr_arena_name=None
        self.vr_arena_file=None
        self.stim_file = None
        self.vid_counter_arena=0

        self.signal=signal

        self.signal = signal
        if self.signal == 'white':
            self.compare = operator.gt
            self.box_intensity = 255
        elif self.signal == 'dark':
            self.compare = operator.lt
            self.box_intensity = 0
        else:
            tk.messagebox.showerror('Error',
                        'Signal has to be either "bright" or "dark".\n'
                        'Please adjust code.\n'
                        'Program will exit after pressing "Ok"')
            import sys
            # Todo: this is overkill! Could also just go back to main Gui
            sys.exit()

        # I think it's better to only read data in here - makes a
        # nicer user experience
        self.raw_images=np.load(self.data_path + '/sm_raw.npy')
        try:
            self.binary_images=np.load(self.data_path + '/sm_thresh.npy')
        except FileNotFoundError:
            self.binary_images = None

        # todo - kick this out at one point - just in because I have
        #  folders with and without that npy file
        try:
            self.bounding_boxes = np.load(self.data_path
                                          + '/bounding_boxes.npy')
        except FileNotFoundError:
            self.bounding_boxes = None

        self.child = tk.Toplevel()
        self.child.grab_set()
        self.child.wm_title('Visualization of the chosen Experiment')

        self.child_frame = tk.Frame(self.child)
        self.child_frame.grid(row=0, column=0)

        # The overview
        self.fig_overview = Figure(figsize=(6,4))
        self.ax_overview = self.fig_overview.add_subplot(111)
        self.image_of_background = self.ax_overview.imshow(
            self.background_image, vmin=0, vmax=255, cmap=self.colormap)

        # turn off the labels and axis to save space
        # self.ax_overview.axes.get_xaxis().set_ticks([])
        # self.ax_overview.axes.get_yaxis().set_ticks([])
        self.ax_overview.grid()
        self.fig_overview.tight_layout()

        # bind the plot to the GUI - do it in a new frame due to the
        # inherent pack method of NaviagationToolbar
        overview_frame = tk.Frame(self.child_frame)
        overview_frame.grid(row=1, column=0, rowspan=3,columnspan=5)
        self.update_overview_button = tk.Button(
            overview_frame,
            text='Updating Overview',
            command=self.update_overview_func)
        self.update_overview_button.pack()

        self.canvas_overview = tkagg.FigureCanvasTkAgg(
            self.fig_overview, master=overview_frame)
        self.canvas_overview.draw()

        self.canvas_overview_background = \
            self.canvas_overview.copy_from_bbox(self.ax_overview.bbox)

        # Add the toolbar
        toolbar = tkagg.NavigationToolbar2Tk(
            self.canvas_overview, overview_frame)
        toolbar.update()

        # The next line is necessary to actually show the figure
        self.canvas_overview.get_tk_widget().pack()

        self.plot_centroid_button = tk.Button(
            overview_frame,
            text='Showing Centroid',
            command=self.plot_centroid_func)
        self.plot_centroid_button.pack(side=tk.LEFT)

        if 'X-Head' in self.csv_data.columns:
            # add a button
            self.plot_head_button = tk.Button(overview_frame,
                                              text='Showing Head',
                                              command=self.plot_head_func)
            self.plot_head_button.pack(side=tk.LEFT)

        if 'X-Tail' in self.csv_data.columns:
            self.plot_tail_button = tk.Button(overview_frame,
                                              text='Showing Tail',
                                              command=self.plot_tail_func)
            self.plot_tail_button.pack(side=tk.LEFT)

        # The raw small image
        self.fig_raw = Figure(figsize=(2,2))
        self.ax_raw = self.fig_raw.add_subplot(111)
        self.image_of_raw_image = self.ax_raw.imshow(
            self.raw_images[:,:,0], vmin=0, vmax=255,
            cmap=self.colormap)

        # turn off the labels and axis to save space
        # self.ax_raw.axes.get_xaxis().set_ticks([])
        # self.ax_raw.axes.get_yaxis().set_ticks([])

        # bind the plot to the GUI
        self.canvas_raw = tkagg.FigureCanvasTkAgg(
            self.fig_raw, master=self.child_frame)
        self.canvas_raw.draw()
        self.canvas_raw.get_tk_widget().grid(row=1, column=6)
        self.canvas_raw_background = \
            self.canvas_raw.copy_from_bbox(self.ax_raw.bbox)

        # The binary small image
        self.fig_binary = Figure(figsize=(2,2))
        self.ax_binary = self.fig_binary.add_subplot(111)
        if self.binary_images is not None:
            self.image_of_binary_image = self.ax_binary.imshow(
                self.binary_images[:,:,0], cmap='Greys_r', vmin=0, vmax=1)
        else:

            # In the pre_experiment.py module I do the following:
            # 1) Save the unfiltered background
            # 2) The background used during tracking is filtered twice:
            #   a) First I use ndimage.filters.gaussian_filter with sigma
            #
            #        minimal_cross_section_animal = \
            #           (self.major_over_minor_axis_min/self.filled_area_min)\
            #               *self.pixel_per_mm
            #       if minimal_cross_section_animal < 2:
            #           #because minimum detectable thickness is one pixel
            #           minimal_cross_section_animal = 2
            #       self.sigma_for_image_filtering = minimal_cross_section_animal/2
            #       if self.sigma_for_image_filtering > 2.5:
            #           self.sigma_for_image_filtering = 2.5 # empirical
            #
            #       This yields the variable self.first_image whic is copied over to
            #       self.goodbackground
            #   b) I then smooth the image again with sigma = 1 which yields
            #       self.smoothed_goodbackground which is used in the
            #       fast_tracking.py module
            """
            # Start by reading the model_organism heuristics
            with open(('\\'.join(os.path.realpath(__file__).split('\\')[:-1])
                       + '/list_of_available_organisms.json'),
                      'r') as file:
                organisms_and_heuristics = json.load(file)

            # Next, read the experiment settings file - not elegant as already
            # read in start_gui.py
            with open(Path(self.data_path, 'experiment_settings.json')) as file:
                experiment_settings = json.load(file)
                model_organism = experiment_settings['Model Organism']
                pixel_per_mm = experiment_settings['Pixel per mm']

            filled_area_min = pixel_per_mm * organisms_and_heuristics[model_organism]['filled_area_min_mm']
            major_over_minor_axis_min = organisms_and_heuristics[model_organism]['major_over_minor_axis_min']

            minimal_cross_section_animal = (major_over_minor_axis_min / filled_area_min) * pixel_per_mm
            # Check comments in pre_experiment.py module for rationale! This is just copy paste
            if minimal_cross_section_animal < 2:
                # because minimum detectable thickness is one pixel
                minimal_cross_section_animal = 2
            sigma_for_image_filtering = minimal_cross_section_animal / 2
            if sigma_for_image_filtering > 2.5:
                sigma_for_image_filtering = 2.5  # empirical
            print('sigma for image_ filtering: ' + repr(sigma_for_image_filtering))

            goodbackground = ndimage.gaussian_filter(
                self.background_image, sigma=sigma_for_image_filtering)
            self.smoothed_goodbackground = ndimage.gaussian_filter(
                goodbackground, sigma=1)
            """
            """
            self.smoothed_goodbackground = imageio.imread(Path(self.data_path, 'Background_smooth.tiff'))

            # Init
            self.current_image_thresholded = None
            # Call the function that calculates the binary image for frame 0
            # This will set self.current_image_thresholded to the correct values
            self.extract_binary_frame(0)
            # display the calculated binary image
            self.image_of_binary_image = self.ax_binary.imshow(
                self.current_image_thresholded, cmap='Greys_r', vmin=0, vmax=1)
            """
            pass


        # turn off the labels and axis to save space
        # self.ax_binary.axes.get_xaxis().set_ticks([])
        # self.ax_binary.axes.get_yaxis().set_ticks([])

        if self.binary_images is not None: # Remove if reconstructed binary images should be shown
            # bind the plot to the GUI
            self.canvas_binary = tkagg.FigureCanvasTkAgg(
                self.fig_binary, master=self.child_frame)
            self.canvas_binary.draw()
            self.canvas_binary.get_tk_widget().grid(row=2, column=6)
            self.canvas_binary_background = \
                self.canvas_binary.copy_from_bbox(self.ax_binary.bbox)

        self.child.update()

        self.play_frame = tk.Frame(self.child_frame,
                                   relief='groove', borderwidth=2)
        self.play_frame.grid(row=6, column=0, columnspan=5, sticky='w')

        # button for play
        self.play_button = tk.Button(self.play_frame,
                                     text='Start Playing',
                                     command=self.play_func)
        self.play_button.grid(row=0,column=0)

        # scale to choose where to play
        self.image_number_scale = tk.Scale(
               self.play_frame,
               from_=0,
               to=self.raw_images.shape[2]-1,
               resolution = 1,
               label='Frame shown',
               variable = self.image_number,
               orient='horizontal',
               len=400,
               command=self.update_visualization)
        self.image_number_scale.grid(row=0, column=1,columnspan=3)
        self.image_number_scale.set(self.image_number.get())

        self.playback_speed_frame = tk.Frame(self.child_frame,
                                             relief='groove',
                                             borderwidth=2)
        self.playback_speed_frame.grid(row=7,
                                       column=0,
                                       columnspan=5,
                                       sticky='w')
        # Information about recording framerate
        self.recording_framerate_label = tk.Label(
            self.playback_speed_frame,
            text = 'Experiment was recorded with '
                   + repr(self.recording_framerate)
                   + 'fps and is being played back at')
        self.recording_framerate_label.grid(row=0, column=0)

        # menu for speed
        self.speed_menu = tk.OptionMenu(self.playback_speed_frame,
                                        self.playback_speed_variable,
                                        *self.playback_speed_options)
        self.speed_menu.grid(row=0, column=1)

        # more info about recording framerate
        self.recording_framerate_label_two = tk.Label(
            self.playback_speed_frame,
            text='speed')
        self.recording_framerate_label_two.grid(row=0, column=2)

        self.manual_jump_frame = tk.Frame(self.child_frame,
                                          relief='groove',
                                          borderwidth=2)
        self.manual_jump_frame.grid(row=8, column=0, sticky='w', columnspan=5)

        # Let user manually enter a frame they want to jump to
        self.manually_jump_to_frame_label = tk.Label(
            self.manual_jump_frame,
            text='Enter a frame you want to jump to')
        self.manually_jump_to_frame_label.grid(row=0, column=0)

        self.manually_jump_to_frame_text = tk.Entry(
            self.manual_jump_frame, width=5)
        self.manually_jump_to_frame_text.grid(row=0, column=1)

        self.manually_jump_to_frame_button = tk.Button(
            self.manual_jump_frame,
            text='Jump to frame',
            command=self.manually_jump_to_frame_func)
        self.manually_jump_to_frame_button.grid(row=0, column=2)

        ####
        # Make a Frame with buttons to allow user to do HT swaps easily
        ###
        if 'X-Head' in self.csv_data.columns:
            self.head_tail_swap_frame = tk.Frame(self.child_frame,
                                                 relief='groove',
                                                 borderwidth=2)
            self.head_tail_swap_frame.grid(row=9, column=0, sticky='w')

            self.ht_swap_label = tk.Label(
                self.head_tail_swap_frame,
                text='Manual Head Tail correction')
            self.ht_swap_label.grid(row=0,column=0, columnspan=3)

            self.ht_start_frame_label = tk.Label(
                self.head_tail_swap_frame,
                text='Start frame')
            self.ht_start_frame_label.grid(row=1, column=0)

            self.ht_start_frame_text = tk.Entry(
                self.head_tail_swap_frame, width=5)
            self.ht_start_frame_text.grid(row=1, column=1)

            self.ht_end_frame_label = tk.Label(
                self.head_tail_swap_frame,
                text='End frame')
            self.ht_end_frame_label.grid(row=2, column=0)

            self.ht_end_frame_text = tk.Entry(
                self.head_tail_swap_frame, width=5)
            self.ht_end_frame_text.grid(row=2, column=1)

            self.ht_swap_button = tk.Button(self.head_tail_swap_frame,
                                         text='Swap Head & Tail',
                                         command=self.manual_ht_swap_func)
            self.ht_swap_button.grid(row=3,column=0, columnspan=3)

        # Moved the automatic HT swap button down after v.1.5.0
        if 'X-Head' in self.csv_data.columns:
            self.auto_head_tail_swap_frame = tk.Frame(self.child_frame,
                                                 relief='groove',
                                                 borderwidth=2)
            self.auto_head_tail_swap_frame.grid(row=9, column=1, sticky='w')

            self.auto_ht_swap_label = tk.Label(
                self.auto_head_tail_swap_frame,
                text='Automatic Head Tail correction:\n'\
                    'Swap Head and Tail between two '\
                    'timepoints where Head and Tail could '\
                    'not be assigned, e.g. during a U-Turn',
            wraplength=190,
            justify=tk.LEFT)
            self.auto_ht_swap_label.grid(row=0,column=0, rowspan=3)

            self.ht_swap_button = tk.Button(
                self.auto_head_tail_swap_frame,
                text='Automatically Swap Head & Tail',
                command = self.auto_ht_swap_func)
            self.ht_swap_button.grid(row=3,column=0)

        ###
        # Head and Tail stitcher
        ###
        self.HT_stitcher_frame = tk.Frame(self.child_frame,
                                          relief='groove',
                                          borderwidth=2)
        self.HT_stitcher_frame.grid(row=9, column=4, rowspan=3)

        self.HT_stitcher_text = tk.Label(
            self.HT_stitcher_frame,
            text='Head and Tail stitcher:\n'
                 'AFTER correcting Head and Tail '
                 'assignment (left) press below to '
                 'interpolate missing Head and Tail '
                 'coordinates.',
            wraplength=200,
            justify=tk.LEFT)
        self.HT_stitcher_text.grid(row=0, column=0)

        self.HT_stitcher_button = tk.Button(
            self.HT_stitcher_frame,
            text='Interpolate H/T coordinates',
            command = self.HT_stitcher_func)
        self.HT_stitcher_button.grid(row=1,column=0)

        ###
        # Make a Frame with buttons to allow user to set values to NaN
        # at the beginning and the end of the experiment if tacking
        # went wrong
        ###
        self.cut_trajectory_frame = tk.Frame(self.child_frame,
                                             relief='groove',
                                             borderwidth=2)
        self.cut_trajectory_frame.grid(row=9, column=6,
                                       rowspan=3)

        self.cut_trajectory_label = tk.Label(
            self.cut_trajectory_frame,
            text='Manual cutting of trajectory')
        self.cut_trajectory_label.grid(row=0, column=0, columnspan=3)

        self.cut_start_frame_label = tk.Label(
            self.cut_trajectory_frame,
            text='Start frame')
        self.cut_start_frame_label.grid(row=1, column=0)

        self.cut_start_frame_text = tk.Entry(
            self.cut_trajectory_frame,
            width=5)
        self.cut_start_frame_text.grid(row=1, column=1)

        self.cut_end_frame_label = tk.Label(
            self.cut_trajectory_frame,
            text='End frame')
        self.cut_end_frame_label.grid(row=2, column=0)

        self.cut_end_frame_text = tk.Entry(
            self.cut_trajectory_frame,
            width=5)
        self.cut_end_frame_text.grid(row=2, column=1)

        self.cut_trajectory_button = tk.Button(
            self.cut_trajectory_frame,
            text='Cut trajectory',
            command=self.cut_trajectory_func)
        self.cut_trajectory_button.grid(row=3, column=0, columnspan=3)

        # save video frame
        self.save_video_frame = tk.LabelFrame(self.child_frame,
                                              text='Save as Video')
        self.save_video_frame.grid(row=6, column=6, rowspan=3)

        # start video recording at frame
        self.video_start_label = tk.Label(self.save_video_frame,
                                          text='Start at Frame')
        self.video_start_label.grid(row=0, column=0)
        self.video_start_frame_entry = tk.Entry(self.save_video_frame,
                                                width=5)
        self.video_start_frame_entry.grid(row=1, column=0)
        self.video_start_frame_entry.insert(tk.END, '0')

        # end video recording at frame
        self.video_end_label = tk.Label(self.save_video_frame,
                                        text='End at Frame')
        self.video_end_label.grid(row=0, column=1)
        self.video_end_frame_entry = tk.Entry(self.save_video_frame, width=5)
        self.video_end_frame_entry.grid(row=1, column=1)
        self.video_end_frame_entry.insert(tk.END,
                                          str(self.raw_images.shape[2]))

        # Button to load VR-Arena
        self.load_vr_arena_button = tk.Button(
            self.save_video_frame,
            text='select VR Arena',
            command=self.load_vr_arena_func)
        self.load_vr_arena_button.grid(row=2, column=0, columnspan=1)

        self.load_stim_file_button = tk.Button(
            self.save_video_frame,
            text='select stim file',
            command=self.load_stim_file_func)
        self.load_stim_file_button.grid(row=2, column=1)

        # variables to have a reference to the text object when
        # saving time dependent stimulus video
        self.ch_one_text = None
        self.ch_two_text = None
        self.ch_three_text = None
        self.ch_four_text = None


        self.loaded_vr_arena_label = tk.Label(self.save_video_frame,
                                              text='None selected')
        self.loaded_vr_arena_label.grid(row=3, column=0, columnspan=2)

        self.update_vr_arena_label = tk.Label(
            self.save_video_frame, text ='Update arena every nth frame')
        self.update_vr_arena_label.grid(row=4, column=0, columnspan=2)

        self.update_vr_arena_entry = tk.Entry(self.save_video_frame,
                                              width=5)
        self.update_vr_arena_entry.grid(row=5, column=0, columnspan=2)
        self.update_vr_arena_entry.config(state='disabled')

        # button to save the video
        self.video_create_button = tk.Button(self.save_video_frame,
                                             text='Save video',
                                             command=self.save_video_func)
        self.video_create_button.grid(row=6, column=0, columnspan=2)

        # Create a new figure that will be used to just plot what the
        # user wants independent of the displayed part
        self.fig_vid = Figure(figsize=(25, 25))  # Todo interactive!
        self.ax_vid = self.fig_vid.add_subplot(111)
        self.ax_vid.axis('off')
        self.image_of_background_vid = self.ax_vid.imshow(
            self.background_image, vmin=0, vmax=255, cmap='Greys_r')
        # Need to call and keep reference even if it's never shown in
        # the canvas
        self.canvas_vid = tkagg.FigureCanvasTkAgg(self.fig_vid,
                                                  master=self.child_frame)
        self.canvas_vid.draw()


        # start by playing the recorded experiment
        self.child.after(0, self.update_visualization())
        self.child.after(0, self.callback_func())

    def load_vr_arena_func(self):
        """
        Function is called when user presses the 'select VR arena'
        button.
        Reads both static (*.csv) and dynamic (*.npy) arenas.
        Currently this data is only used to create videos.
        """
        if self.data_path is not None:
            self.vr_arena_name = filedialog.askopenfilename(
                initialdir=self.data_path)
        else:
            self.vr_arena_name = filedialog.askopenfilename()
        if 'csv' in self.vr_arena_name:
            self.vr_arena_file = np.genfromtxt(self.vr_arena_name, delimiter=',')
            path_to_show = '...' + self.vr_arena_name[-30:]
            self.loaded_vr_arena_label.configure(text=path_to_show)

            self.load_vr_arena_button.config(fg='red')
            self.load_stim_file_button.config(fg='grey')

            self.update_vr_arena_entry.config(state='disabled')

            self.stim_file = None
        elif 'npy' in self.vr_arena_name:
            self.vr_arena_file=np.load(self.vr_arena_name)
            path_to_show = '...' + self.vr_arena_name[-30:]
            self.loaded_vr_arena_label.configure(text=path_to_show)

            self.load_vr_arena_button.config(fg='red')
            self.load_stim_file_button.config(fg='grey')

            self.update_vr_arena_entry.config(state='normal')

            self.stim_file = None
        else:
            tk.messagebox.showerror('File can not be read',
                                    'Please select a PiVR VR arena '
                                    'file.')

    def load_stim_file_func(self):
        """
        Function is called when user presses the 'select stim file'
        button.
        Reads the csv files associated with time dependent stimulus.
        Currently, this data is only used to create videos.
        """
        if self.data_path is not None:
            self.stim_file_name = filedialog.askopenfilename(
                initialdir=self.data_path)
        else:
            self.stim_file_name = filedialog.askopenfilename()

        if 'csv' in self.stim_file_name:
            self.stim_file = pd.read_csv(self.stim_file_name)

            # some checks that the file is in the correct format:
            if 'Channel 1' in self.stim_file and \
                'Channel 2' in self.stim_file.columns and \
                'Channel 3' in self.stim_file.columns and \
                'Channel 4' in self.stim_file.columns:

                path_to_show = '...' + self.stim_file_name[-30:]
                self.loaded_vr_arena_label.configure(text=path_to_show)
                self.load_stim_file_button.config(fg='red')
                self.load_vr_arena_button.config(fg='grey')

                self.update_vr_arena_entry.config(state='disabled')

                self.vr_arena_file = None

            else:
                tk.messagebox.showerror('Incorrect file',
                                        'Your time dependent stimulus '
                                        'file must have 4 Channels!'
                                        )
        else:
            tk.messagebox.showerror('File can not be read',
                                    'Please select a PiVR stimulus '
                                    'file.')

    def save_video_func(self):
        """
        Function that is called when the user hits the 'Save Video'
        button. Needs FFMPEG installed on the computer to work.
        Unsure about the error message - as soon s I can test it
        implement a error message asking the user to install ffmpeg.
        """
        # Reset the self.vid_counter_arena
        self.vid_counter_arena = 0

        # Set up formatting for the movie files
        writer = animation.FFMpegWriter(fps=self.recording_framerate,
                                        bitrate=5000)

        # if user puts a higher number in start entry widget compared
        # to the end entry widget, just swap the two and inform the user
        if int(self.video_start_frame_entry.get()) \
                > int(self.video_end_frame_entry.get()):
            messagebox.showinfo('Start Frame Larger Than End Frame',
                                'You have chosen a start frame of '
                                + self.video_start_frame_entry.get()
                                + '\nand a end frame of '
                                + self.video_end_frame_entry.get() +
                                '.\nTo create the video these values '
                                'were swapped')
            temp_end = self.video_end_frame_entry.get()
            temp_start = self.video_start_frame_entry.get()
            self.video_end_frame_entry.delete(0, tk.END)
            self.video_start_frame_entry.delete(0, tk.END)
            self.video_end_frame_entry.insert(tk.END, temp_start)
            self.video_start_frame_entry.insert(tk.END, temp_end)

        # If user put a larger number than there are frames,
        # just reset the end frame and let the user know
        if int(self.video_end_frame_entry.get()) \
                > self.raw_images.shape[2]:
            messagebox.showinfo('Invalid End Frame',
                                'You have chosen '
                                + self.video_end_frame_entry.get()
                                + ' as the end frame.'
                                  '\nThe  experiment only consists of '
                                + repr(self.raw_images.shape[2]) +
                                ' images.'
                                '\nThe video will be created '
                                'with end frame '
                                + repr(self.raw_images.shape[2]))
            self.video_end_frame_entry.delete(0, tk.END)
            self.video_end_frame_entry.insert(tk.END, self.raw_images.shape[2])

        # When updating the percentage of the stimulus, I need to
        # keep a reference to the original text object. To make sure
        # it's only created once, create here just before making the
        # call to the animation function
        if self.stim_file is not None:
            textsize=22
            self.ch_one_text = self.ax_vid.text(10,50, '',
                                                fontsize=textsize)
            self.ch_two_text = self.ax_vid.text(70,50, '',
                                                fontsize=textsize)
            self.ch_three_text = self.ax_vid.text(130,50, '',
                                                fontsize=textsize)
            self.ch_four_text = self.ax_vid.text(190,50, '',
                                                fontsize=textsize)

        animated_experiment = animation.FuncAnimation(
            self.fig_vid, self.update_for_video_func,
            frames=int(self.video_end_frame_entry.get())
                   - int(self.video_start_frame_entry.get()),
            interval=int(round(1000/self.recording_framerate)),
            blit=True, repeat=False,
                                        )

        animated_experiment.save('Video.mp4', writer=writer)

    def update_for_video_func(self, frame):
        """
        :param frame: Given by the FuncAnimation class from
        matpotlib.  Essentially an iterator that starts at 0 and goes
        up to the maximum frame defined by the user. This is
        :return:
        """

        # make a RGB image
        image_to_plot = np.zeros((self.background_image.shape[0],
                                  self.background_image.shape[1],
                                  3))
        # insert the image into all 3 channels
        for i in range(3):
            image_to_plot[:,:,i] = self.background_image.copy()

        if self.vr_arena_file is not None:
            if len(self.vr_arena_file.shape) == 2:
                if frame==0:
                    self.ax_vid.imshow(self.vr_arena_file, alpha=.1)
            elif len(self.vr_arena_file.shape) == 3:
                if self.update_vr_arena_entry.get() is not None:
                    if frame % int(self.update_vr_arena_entry.get()) == 0:
                        # update the image
                        self.ax_vid.cla()
                        self.image_of_background_vid = \
                            self.ax_vid.imshow(
                                self.background_image,
                                vmin=0,
                                vmax=255,
                                cmap='Greys_r')
                        try:
                            self.ax_vid.imshow(
                                self.vr_arena_file[:,:,
                                self.vid_counter_arena], alpha=.2)
                            self.vid_counter_arena += 1
                        except IndexError:
                            messagebox.showwarning(
                                'IndexError',
                                'Index 101 is out of bounds for axis '
                                '2 with size 101.'
                                '\nThis means the VR arena file you '
                                'selected does not'
                                '\nhave enough images with the '
                                'update rate you selected.'
                                '\nReseting counter and starting at '
                                'first arena')
                            self.vid_counter_arena = 0

        elif self.stim_file is not None:
            # Can't do this here, need to get the updated frame
            # parameter! Done below
            pass

        frame += int(self.video_start_frame_entry.get())
        print(frame)

        if self.stim_file is not None:
            textsize = 25

            self.ax_vid.text(10, 20,'Ch 1',fontsize=textsize)
            self.ch_one_text.set_text(
                repr(self.stim_file['Channel 1'][frame]) + '%')

            rr,cc = draw.rectangle(start=(30, 0),
                                   extent=(30,50))
            image_to_plot[rr,cc,0] = self.stim_file['Channel 1'][frame]\
                                     /(100/255)

            self.ax_vid.text(70,20,'Ch 2',fontsize=textsize)
            self.ch_two_text.set_text(
                repr(self.stim_file['Channel 2'][frame]) + '%')
            rr,cc = draw.rectangle(start=(30, 60),
                                   extent=(30,50))
            image_to_plot[rr,cc,0] = self.stim_file['Channel 2'][frame]\
                                     /(100/255)

            self.ax_vid.text(130,20,'Ch 3',fontsize=textsize)
            self.ch_three_text.set_text(
                repr(self.stim_file['Channel 3'][frame]) + '%')
            rr,cc = draw.rectangle(start=(30, 120),
                                   extent=(30,50))
            image_to_plot[rr,cc,0] = self.stim_file['Channel 3'][frame]\
                                     /(100/255)

            self.ax_vid.text(190,20,'Ch 4',fontsize=textsize)
            self.ch_four_text.set_text(
                repr(self.stim_file['Channel 4'][frame]) + '%')
            rr,cc = draw.rectangle(start=(30, 180),
                                   extent=(30,50))
            image_to_plot[rr,cc,0] = self.stim_file['Channel 4'][frame]\
                                     /(100/255)

            '''
            # Create a Rectangle patch
            rect_ch1 = mpatches.Rectangle((0, 0), 30, 20, linewidth=1,
                                     edgecolor='r', facecolor='b')

            # Add the patch to the Axes
            self.ax_vid.add_patch(rect_ch1)

            rect_ch2 = mpatches.Rectangle((30,0),30,20, linewidth=1,
                                     edgecolor='r', facecolor='b')
            self.ax_vid.add_patch(rect_ch2)

            rect_ch3 = mpatches.Rectangle((60,0),30,20, linewidth=1,
                                     edgecolor='r', facecolor='b')
            self.ax_vid.add_patch(rect_ch3)

            rect_ch4 = mpatches.Rectangle((90,0),30,20, linewidth=1,
                                     edgecolor='r', facecolor='b')
            self.ax_vid.add_patch(rect_ch4)
            '''

        for i in range(3):
            # Todo, remove at one point
            if self.bounding_boxes is not None or \
                'Ymin-bbox' in self.csv_data:
                if self.bounding_boxes is not None:
                    image_to_plot[self.bounding_boxes[0, frame]:
                                  self.bounding_boxes[1, frame], \
                                  self.bounding_boxes[2, frame]:
                                  self.bounding_boxes[3, frame],
                                  i] = \
                        self.raw_images[0:int(self.bounding_boxes[1, frame]
                                            - self.bounding_boxes[0, frame]),
                                            0:int(self.bounding_boxes[3, frame]
                                            - self.bounding_boxes[2, frame]),
                                        frame]
                else:
                    image_to_plot[self.csv_data['Ymin-bbox'][frame]:
                                  self.csv_data['Ymax-bbox'][frame], \
                                  self.csv_data['Xmin-bbox'][frame]:
                                  self.csv_data['Xmax-bbox'][frame],
                                  i] = \
                    self.raw_images[0:int(self.csv_data['Ymax-bbox'][frame]
                                        - self.csv_data['Ymin-bbox'][frame]),
                                    0:int(self.csv_data['Xmax-bbox'][frame]
                                        - self.csv_data['Xmin-bbox'][frame]),
                                    frame]

        if self.undistort_data is not None:
            # Probably easiest to do undistort/remap of whole image here
            image_to_plot = cv2.remap(image_to_plot, self.map1, self.map2,
                                        interpolation=cv2.INTER_LINEAR,
                                        # borderMode=cv2.BORDER_REFLECT_101,
                                        # borderValue=None
                                        )

        if self.plot_heads:
            rr,cc = draw.circle(r=self.csv_data['Y-Head'][frame],
                                c=self.csv_data['X-Head'][frame],
                                radius=5)
            image_to_plot[rr,cc,0] = 255

            self.ax_overview.draw_artist(self.image_scatter_head)

        if self.plot_tails:
            rr,cc = draw.circle(r=self.csv_data['Y-Tail'][frame],
                                c=self.csv_data['X-Tail'][frame],
                                radius=5)
            image_to_plot[rr,cc,1] = 255

        if self.plot_centroids:
            rr,cc = draw.circle(r=self.csv_data['Y-Centroid'][frame],
                                c=self.csv_data['X-Centroid'][frame],
                                radius=5)
            image_to_plot[rr,cc,2] = 255

        self.image_of_background_vid.set_data(image_to_plot.astype(np.uint8))

        return self.image_of_background_vid,

    def update_visualization(self, scale_input=None):
        """
        Updates the embedded matplotlib plots by setting the data to
        the current image_number
        """
        if self.update_overview_bool:
            if self.bounding_boxes is not None or \
                    'Ymin-bbox' in self.csv_data.columns:

                self.image_to_plot = self.background_image.copy()
                # Todo - remove sometime in the future
                if self.bounding_boxes is not None:

                    self.image_to_plot[
                        self.bounding_boxes[0,self.image_number.get()]:
                        self.bounding_boxes[1,self.image_number.get()],
                        self.bounding_boxes[2,self.image_number.get()]:
                        self.bounding_boxes[3,self.image_number.get()]
                                        ] = \
                        self.raw_images[
                            0:int(self.bounding_boxes[1,self.image_number.get()]
                          - self.bounding_boxes[0, self.image_number.get()]),
                            0:int(self.bounding_boxes[3,self.image_number.get()]
                          - self.bounding_boxes[2,self.image_number.get()]),
                        self.image_number.get()]
                else:
                    self.image_to_plot[int(self.csv_data['Ymin-bbox'][self.image_number.get()]):
                                       int(self.csv_data['Ymax-bbox'][self.image_number.get()]), \
                                       int(self.csv_data['Xmin-bbox'][self.image_number.get()]):
                                       int(self.csv_data['Xmax-bbox'][self.image_number.get()]),
                                        ] = \
                        self.raw_images[
                                0:int(self.csv_data['Ymax-bbox'][self.image_number.get()]
                                      - self.csv_data['Ymin-bbox'][self.image_number.get()]),
                                0:int(self.csv_data['Xmax-bbox'][self.image_number.get()]
                                      - self.csv_data['Xmin-bbox'][self.image_number.get()]),
                                self.image_number.get()]

                if self.undistort_data is not None:
                    print('updating')
                    # Probably easiest to do undistort/remap of whole image here
                    self.image_to_plot = cv2.remap(self.image_to_plot, self.map1,
                                                  self.map2,
                                                  interpolation=cv2.INTER_LINEAR,
                                                  # borderMode=cv2.BORDER_REFLECT_101,
                                                  # borderValue=None
                                                  )
                print('not updating')


                self.image_of_background.set_data(self.image_to_plot)

                self.canvas_overview.restore_region(
                    self.canvas_overview_background)
                self.ax_overview.draw_artist(self.image_of_background)

                if self.initalize:
                    if self.plot_heads:
                        self.image_scatter_head = \
                            self.ax_overview.scatter(
                                x=self.csv_data['X-Head'][
                                    self.image_number.get()],
                                y=self.csv_data['Y-Head'][
                                    self.image_number.get()],
                                color='r', label='Head')

                        self.ax_overview.draw_artist(self.image_scatter_head)

                    if self.plot_tails:
                        self.image_scatter_tail = \
                            self.ax_overview.scatter(
                                x=self.csv_data['X-Tail'][
                                     self.image_number.get()],
                                y=self.csv_data['Y-Tail'][
                                     self.image_number.get()],
                                color='k', label='Tail')
                        self.ax_overview.draw_artist(
                            self.image_scatter_tail)

                    if self.plot_centroids:
                        self.image_scatter_centroid = \
                            self.ax_overview.scatter(
                                x=self.csv_data['X-Centroid'][
                                     self.image_number.get()],
                                y=self.csv_data['Y-Centroid'][
                                     self.image_number.get()],
                                color='b', label='Centroid')
                        self.ax_overview.draw_artist(
                            self.image_scatter_centroid)

                    self.canvas_overview.blit(self.ax_overview.bbox)
                    self.ax_overview.legend()

                    self.initalize = False
                else:
                    if self.plot_heads:
                        self.image_scatter_head.set_offsets(
                            np.hstack((self.csv_data['X-Head'][
                                            self.image_number.get()],
                                       self.csv_data['Y-Head'][
                                           self.image_number.get()]))
                                        )
                        self.ax_overview.draw_artist(
                            self.image_scatter_head)
                    if self.plot_tails:
                        self.image_scatter_tail.set_offsets(
                            np.hstack((self.csv_data['X-Tail'][
                                            self.image_number.get()],
                                        self.csv_data['Y-Tail'][
                                             self.image_number.get()]))
                                            )
                        self.ax_overview.draw_artist(
                            self.image_scatter_tail)

                    if self.plot_centroids:
                        self.image_scatter_centroid.set_offsets(
                            np.hstack((self.csv_data['X-Centroid'][
                                            self.image_number.get()],
                                        self.csv_data['Y-Centroid'][
                                             self.image_number.get()]))
                                                )

                        self.ax_overview.draw_artist(
                            self.image_scatter_centroid)
                    self.canvas_overview.blit(self.ax_overview.bbox)

            else:
                # todo - shouldn't be necessary anymore as we always
                #  save the bounding box from now on
                if self.initalize:
                    self.image_scatter = \
                        self.ax_overview.scatter(
                            x=self.csv_data['X-Centroid'][
                                 self.image_number.get()],
                            y=self.csv_data['Y-Centroid'][
                                 self.image_number.get()],
                            color='k', marker='o')
                    self.canvas_overview.draw()
                    self.initalize = False
                else:
                    self.image_scatter.set_offsets(
                        np.hstack((self.csv_data['X-Centroid'][
                                        self.image_number.get()],
                                    self.csv_data['Y-Centroid'][
                                         self.image_number.get()]))
                                    )
                    # blitting as draw would take too long
                    self.canvas_overview.restore_region(self.canvas_overview_background)
                    self.ax_overview.draw_artist(self.image_scatter)
                    self.canvas_overview.blit(self.ax_overview.bbox)

        self.image_of_raw_image.set_data(
            self.raw_images[:, :, self.image_number.get()])
        # blitting as draw would take too long
        self.canvas_raw.restore_region(self.canvas_raw_background)
        self.ax_raw.draw_artist(self.image_of_raw_image)
        self.canvas_raw.blit(self.ax_raw.bbox)

        if self.binary_images is not None:
            self.image_of_binary_image.set_data(
                self.binary_images[:, :, self.image_number.get()])
        else:
            """
            # Calling the extract_binary_frame function sets self.current_image_thresholded
            # to the correct values
            self.extract_binary_frame(self.image_number.get())
            self.image_of_binary_image.set_data(
                self.current_image_thresholded
            )
            """
        if self.binary_images is not None: # Remove if reconstructed binary images should be shown
            # blitting as draw would take too long
            self.canvas_binary.restore_region(self.canvas_binary_background)
            self.ax_binary.draw_artist(self.image_of_binary_image)
            self.canvas_binary.blit(self.ax_raw.bbox)

        self.child.update()

    def play_func(self):
        if self.play_button['text'] == 'Stop Playing':
            self.play_button['text'] = 'Start Playing'
        else:
            self.play_button['text'] = 'Stop Playing'
            #self.child.after_cancel(self.callback_func_variable)
            while self.play_button['text'] == 'Stop Playing':
                if self.image_number.get() < self.raw_images.shape[2]:
                    time_start = time.time()
                    self.update_visualization()
                    self.image_number.set(self.image_number.get() + 1)

                    # play as fast as requested
                    if (time.time() - time_start) *1000 \
                            <1000/self.recording_framerate\
                            *(1/self.playback_speed_value):
                        print(repr(int(round(((
                              1000/self.recording_framerate
                              *(1/self.playback_speed_value))
                              - (time.time() - time_start))))))

                        self.child.after(int(round(((
                                1000/self.recording_framerate
                                *(1/self.playback_speed_value))
                                - (time.time() - time_start)))))
                    else:
                        print(time.time() - time_start)# notify the user somehow
                else:
                    self.image_number.set(0)

    def callback_func(self):
        self.child.after(700, self.callback_func)
        if self.playback_speed_variable.get() != 'Custom':
            if self.playback_speed_value != \
                    float(self.playback_speed_variable.get()[:-1]):
                self.playback_speed_value = \
                    float(self.playback_speed_variable.get()[:-1])
        else:
            print('hi')
            # Todo open popup asking user for a custom speed

    def manually_jump_to_frame_func(self):
        try:
            if 0 < int(self.manually_jump_to_frame_text.get()) \
                    < self.raw_images.shape[2]:
                self.image_number.set(int(
                    self.manually_jump_to_frame_text.get()))
                self.update_visualization()
            else:
                messagebox.showerror(
                    "Invalid Input",
                     "You have entered a value smaller than 0 or "
                     "\nlarger than the exisiting number of frames ("
                     + repr(self.raw_images.shape[2]-1)
                     +") \n "
                      "\n Please enter a number between 0 and "
                     + repr(self.raw_images.shape[2]))

        except ValueError:
            messagebox.showerror(
                "Invalid Input",
                "You have not entered an Integer number. "
                "\n \n Please enter a number between 0 and "
                + repr(self.raw_images.shape[2]-1))

    def plot_head_func(self):
        if self.plot_head_button['text'] == 'Not showing Head':
            self.plot_head_button['text'] = 'Showing Head'
            self.plot_heads = True
            if self.play_button['text'] == 'Start Playing':
                self.update_visualization()
        else:
            self.plot_head_button['text'] = 'Not showing Head'
            self.plot_heads = False
            if self.play_button['text'] == 'Start Playing':
                self.update_visualization()

    def plot_centroid_func(self):
        if self.plot_centroid_button['text'] == 'Not showing Centroid':
            self.plot_centroid_button['text'] = 'Showing Centroid'
            self.plot_centroids = True
            if self.play_button['text'] == 'Start Playing':
                self.update_visualization()
        else:
            self.plot_centroid_button['text'] = 'Not showing Centroid'
            self.plot_centroids = False
            if self.play_button['text'] == 'Start Playing':
                self.update_visualization()

    def plot_tail_func(self):
        if self.plot_tail_button['text'] == 'Not showing Tail':
            self.plot_tail_button['text'] = 'Showing Tail'
            self.plot_tails = True
            if self.play_button['text'] == 'Start Playing':
                self.update_visualization()
        else:
            self.plot_tail_button['text'] = 'Not showing Tail'
            self.plot_tails = False
            if self.play_button['text'] == 'Start Playing':
                self.update_visualization()

    def auto_ht_swap_func(self):
        """
        If user presses the 'Swap Head Tail' button the head and tail
        will be swapped up to the next time the centroid
        is identical to the head and tail which happens for example
        during a larval U-Turn
        """
        current_frame = self.image_number.get()
        # go forward in time until head == tail
        counter = current_frame
        while counter < self.csv_data['X-Head'].shape[0]:
            if not (self.csv_data['X-Head'][counter]
                    == self.csv_data['X-Tail'][counter]
                    and self.csv_data['Y-Head'][counter]
                    == self.csv_data['Y-Tail'][counter]):
                counter+=1
                if counter == self.csv_data['X-Head'].shape[0]:
                    max_value = counter
            else:
                max_value = counter
                break

        # go backward in time until tail == head
        counter = current_frame
        while counter > 0:
            if not (self.csv_data['X-Head'][counter]
                    == self.csv_data['X-Tail'][counter]
                    and  self.csv_data['Y-Head'][counter]
                    == self.csv_data['Y-Tail'][counter]):
                counter-=1
                if counter == 0:
                    min_value = counter
            else:
                min_value = counter
                break
        print('min value ' + repr(min_value))
        print('max value ' + repr(max_value))

        temp_tail_x = np.asarray(
            self.csv_data['X-Head'][min_value:max_value]).copy()
        temp_tail_y = np.asarray(
            self.csv_data['Y-Head'][min_value:max_value]).copy()

        self.csv_data['X-Head'][min_value:max_value] = \
            self.csv_data['X-Tail'][min_value:max_value].copy()
        self.csv_data['Y-Head'][min_value:max_value] = \
            self.csv_data['Y-Tail'][min_value:max_value].copy()

        self.csv_data['X-Tail'][min_value:max_value] = temp_tail_x.copy()
        self.csv_data['Y-Tail'][min_value:max_value] = temp_tail_y.copy()

        self.save_csv_file()
        """
        files = [p.replace('\\', '') for p in glob('*')]
        for i in files:
            if 'data.csv' in i:
                datafile_name = i
        self.csv_data.to_csv(self.data_path + '//' + datafile_name,
                        sep=',',
                        columns = ['Frame',
                                   'Time',
                                   'X-Centroid',
                                   'Y-Centroid',
                                   'X-Head',
                                   'Y-Head',
                                   'X-Tail',
                                   'Y-Tail',
                                   'X-Midpoint',
                                   'Y-Midpoint',
                                   'stimulation'
                                   ]
                        )
        """

        # This is if user asked explicity for the npy files to be saved.
        # only save it if it exists in the first place!
        files = [p.replace('\\', '') for p in glob('*')]
        if 'heads.npy' in files and 'tails.npy' in files:
            # also save in the npy files
            heads = np.load(self.data_path + '//heads.npy')
            tails = np.load(self.data_path + '//tails.npy')

            if not 'original_heads.npy' in files:
                np.save(self.data_path + '//original_heads.npy', heads)
                np.save(self.data_path + '//original_tails.npy', tails)

            temp_heads = heads[min_value:max_value,:].copy()
            heads[min_value:max_value] = tails[min_value:max_value,:].copy()
            tails[min_value:max_value] = temp_heads.copy()

            np.save(self.data_path + '//heads', heads)
            np.save(self.data_path + '//tails', tails)

    def manual_ht_swap_func(self):
        """
        While the auto_ht_swap_func can take care of head tail swaps
        which are due to wrong classification after a turn there are
        many more possibilites to get a head tail swap.

        This function takes the values in ht_start_frame_text and
        ht_end_frame_text and applies head tail swap just between
        these defined frames.
        """
        # This bool makes sure everthing is in order before swapping
        # head and tail.
        continue_analysis = False
        try:
            start = int(self.ht_start_frame_text.get())
            end = int(self.ht_end_frame_text.get())
            continue_analysis = True
        except ValueError:
            tk.messagebox.showerror('Error',
                'You must enter a number in both "Start frame"\n' \
                'and "End frame".')
        if start < end:
            continue_analysis = True
        else:
            tk.messagebox.showerror('Error',
                'The number in "Start frame" must be smaller than\n'\
                'the number in "End frame"')

        if continue_analysis:
            temp_tail_x = np.asarray(
                self.csv_data['X-Head'][start:end]).copy()
            temp_tail_y = np.asarray(
                self.csv_data['Y-Head'][start:end]).copy()

            self.csv_data['X-Head'][start:end] = \
                self.csv_data['X-Tail'][start:end].copy()
            self.csv_data['Y-Head'][start:end] = \
                self.csv_data['Y-Tail'][start:end].copy()

            self.csv_data['X-Tail'][start:end] = temp_tail_x.copy()
            self.csv_data['Y-Tail'][start:end] = temp_tail_y.copy()

            files = [p.replace('\\', '') for p in glob('*')]
            for i in files:
                if 'data.csv' in i:
                    datafile_name = i
            self.save_csv_file()
            """
            # The below shouldn't be necessary as we're not writting new
            # columns!
            if 'stimulation' in self.csv_data:
                self.csv_data.to_csv(self.data_path + '//' + datafile_name,
                                     sep=',',
                                     columns=['Frame',
                                              'Time',
                                              'X-Centroid',
                                              'Y-Centroid',
                                              'X-Head',
                                              'Y-Head',
                                              'X-Tail',
                                              'Y-Tail',
                                              'X-Midpoint',
                                              'Y-Midpoint',
                                              'stimulation'
                                              ]
                                     )
            else:
                self.csv_data.to_csv(self.data_path + '//' + datafile_name,
                                     sep=',',
                                     columns=['Frame',
                                              'Time',
                                              'X-Centroid',
                                              'Y-Centroid',
                                              'X-Head',
                                              'Y-Head',
                                              'X-Tail',
                                              'Y-Tail',
                                              'X-Midpoint',
                                              'Y-Midpoint',
                                              'Stim Ch1',
                                              'Stim Ch2',
                                              'Stim Ch3',
                                              'Stim Ch4'
                                              ]
                                     )
            """
            try:
                # also save in the npy files
                heads = np.load(self.data_path + '//heads.npy')
                tails = np.load(self.data_path + '//tails.npy')

                if not 'original_heads.npy' in files:
                    np.save(self.data_path + '//original_heads.npy', heads)
                    np.save(self.data_path + '//original_tails.npy', tails)

                temp_heads = heads[start:end, :].copy()
                heads[start:end] = tails[start:end, :].copy()
                tails[start:end] = temp_heads.copy()

                np.save(self.data_path + '//heads', heads)
                np.save(self.data_path + '//tails', tails)
            except FileNotFoundError:
                # Newer version of PiVR doesn't save head.npy and tails.npy
                # unless user explicitly ask for it.
                pass

    def update_overview_func(self):
        if self.update_overview_button['text'] == 'Not updating Overview':
            self.update_overview_button['text'] = 'Updating Overview'
            self.update_overview_bool = True
            if self.play_button['text'] == 'Start Playing':
                self.update_visualization()
        else:
            self.update_overview_button['text'] = 'Not updating Overview'
            self.update_overview_bool = False

    def extract_binary_frame(self, i_frame):
        """
        THIS FUNCTION IS CURRENTLY NOT IN USE.
        Just keep here in case it gets re-implemented

        This function mimics the calculation of the binary image in fast tracking in case
        user does not save the binary image during tracking
        :return:
        """
        # need to extract the actual image from the raw_images
        row_min = self.csv_data['Ymin-bbox'][i_frame]
        row_max = self.csv_data['Ymax-bbox'][i_frame]
        col_min = self.csv_data['Xmin-bbox'][i_frame]
        col_max = self.csv_data['Xmax-bbox'][i_frame]

        # filter the image to get rid of camera noise. Only take the
        # search box
        smooth_current_local_image = ndimage.filters.gaussian_filter(
            self.raw_images[0:row_max-row_min, 0:col_max-col_min, i_frame], sigma=1)

        smooth_background_local_image = thc.CallBoundingBox(
            self.smoothed_goodbackground,[row_min, row_max, col_min, col_max]).sliced_image
        try:
            subtracted_current_frame = smooth_current_local_image.astype(
                np.int16) - smooth_background_local_image.astype(np.int16)

            self.current_image_thresholded = np.zeros((self.raw_images.shape[0],
                                                       self.raw_images.shape[1]),dtype=np.bool_)

            # calculate the binary local image
            self.current_image_thresholded[0:row_max-row_min, 0:col_max-col_min] = \
                self.compare(subtracted_current_frame, self.csv_data['local threshold'][i_frame]).copy()
        except Exception as e:
            print(e)
            # The first couple of frames in post-hoc mode are always just zeros.
            # Just display a black image as nothing is detected there.
            self.current_image_thresholded = self.raw_images[:,:,i_frame].copy()

    def cut_trajectory_func(self):
        """
        It might be desirable to cut trajectories: For example when the
        animal hits the wall the tracking algorithm might lose the
        animal and instead of throwing an error might continue tracking
        some part of the experimental platform.

        This can very easily be confirmed with the 'Display Tracked
        Experiment' option in PiVR.

        The function here is called when the user enters a start and end
        frame hits the 'Cut Trajectory' button.

        It first checks if the numbers that have been entered are
        integers.

        If yes, it will save the original data.csv file (which might
        have been modified, e.g. with head/tail corrections) as
        **dataOLD.csv in case the user accidently deletes too much of
        the trajectory.

        It then fills all the rows (frames) that are selected to be cut
        with np.nan.

        Finally, the new csv file is saved as the original one.
        """
        continue_analysis = False

        try:
            start = int(self.cut_start_frame_text.get())
            end = int(self.cut_end_frame_text.get())
            continue_analysis = True
        except ValueError:
            tk.messagebox.showerror('Error',
                'You must enter a number in both "Start frame"\n' \
                'and "End frame".')
        if start < end:
            continue_analysis = True
        else:
            tk.messagebox.showerror('Error',
                'The number in "Start frame" must be smaller than\n'\
                'the number in "End frame"')

        if continue_analysis:
            # Here, the filenames are defined
            no_old_csv = True
            # Since there's the suboptimal naming convention when
            # collecting a video of 'data.csv' to denote info about the
            # stimulus and when then doing analysis it's also called
            # 'data.csv' the loop below first collects all data.csv names
            # and selects the newest one (as analysis must have been done
            # after the data collection).
            files = [p.replace('\\', '') for p in glob('*')]

            files_of_interest = []
            for i in files:
                if 'data.csv' in i:
                    files_of_interest.append(i)
                    # data_name = i
            if len(files_of_interest) == 1:
                datafile_name = files_of_interest[0]
            else:
                files_of_interest.sort()
                datafile_name = files_of_interest[-1]

            for current_file in files:
                if 'dataOLD.csv' in current_file:
                    no_old_csv = False # Make sure to not accidentally
                    # overwrite the original file
            # Only write the OLD.csv file if it doesn't exist yet to
            # avoid overwriting the original data!
            if no_old_csv:
                print('writing "dataOLD.csv" file')
                datafile_name_OLD = datafile_name.split('.csv')[0] + 'OLD' + '.csv'
                if no_old_csv:
                    self.csv_data.to_csv(
                        self.data_path + '//' + datafile_name_OLD,
                        sep=',')

            # After making sure the orignal data is save, take user
            # input and set all values that are NOT in range selected
            # by user
            remove_data_start_indeces = np.arange(0, start) # if 0 is entered nothing will happen
            remove_data_end_indeces = np.arange(end, self.csv_data.shape[0])

            self.csv_data.loc[remove_data_start_indeces] = np.nan
            self.csv_data.loc[remove_data_end_indeces] = np.nan

            self.save_csv_file()

            print('successfully updated data.csv file')

    def HT_stitcher_func(self):
        """
        Problem:

        When the animal is rolling up and during challenging image conditions
        head and tail can't be assigned.

        After making sure head and tail are correctly identified it's a good
        idea to find instances where head and tail are the same as the centroid
        and interpolate the missing data
        """
        # find duplicates
        duplicates = np.where((self.csv_data['X-Centroid'] == self.csv_data['X-Head']) &
                              (self.csv_data['Y-Centroid'] == self.csv_data['Y-Head']) &
                              (self.csv_data['X-Centroid'] == self.csv_data['X-Tail']) &
                              (self.csv_data['Y-Centroid'] == self.csv_data['Y-Tail']))[0]

        # Go through the duplicates
        for current_duplicate in self.consecutive(duplicates):
            # only correct if not at the very beginning and not at the very end!
            if current_duplicate[0] != 0 and current_duplicate[-1] != self.csv_data.shape[0]-1:
                self.csv_data = self.interpolate_values(body_part='X-Head', csv=self.csv_data,
                                          i=current_duplicate)
                self.csv_data = self.interpolate_values(body_part='Y-Head', csv=self.csv_data,
                                          i=current_duplicate)
                self.csv_data = self.interpolate_values(body_part='X-Tail', csv=self.csv_data,
                                          i=current_duplicate)
                self.csv_data = self.interpolate_values(body_part='Y-Tail', csv=self.csv_data,
                                          i=current_duplicate)

        # Save the data.csv file
        self.save_csv_file()

    def consecutive(self, data, stepsize=1):
        return np.split(data, np.where(np.diff(data) != stepsize)[0] + 1)

    def interpolate_values(self, body_part, csv, i):
        """
        This function takes the data before and after the curl and
        interpolates the value and puts it back into the data file
        """
        # y values to use for interpolation
        before = csv[body_part][i[0] - 1]
        after = csv[body_part][i[-1] + 1]
        fp = [before, after]

        # index of values to be interpolated
        xvals = np.arange(1, i.shape[0] + 1, 1)
        # index of values before and after the value to be interpolated
        xp = [0, i.shape[0] + 1]

        csv[body_part][i] = np.interp(xvals, xp, fp).copy()
        return (csv)

    def save_csv_file(self):
        """
        There are a bunch of times where the data.csv must be re-saved.

        To avoid having slightly different ways of how it's saved, use
        this function.
        """
        files = [p.replace('\\', '') for p in glob('*')]

        # Since there's the suboptimal naming convention when
        # collecting a video of 'data.csv' to denote info about the
        # stimulus and when then doing analysis it's also called
        # 'data.csv' the loop below first collects all data.csv names
        # and selects the newest one (as analysis must have been done
        # after the data collection).
        files_of_interest = []
        for i in files:
            if 'data.csv' in i:
                files_of_interest.append(i)
                #data_name = i
        if len(files_of_interest) == 1:
            datafile_name = files_of_interest[0]
        else:
            files_of_interest.sort()
            datafile_name = files_of_interest[-1]
        print(datafile_name)
        try:
            self.csv_data.drop('Unnamed: 0', axis=1, inplace=True)
        except KeyError:
            pass
        self.csv_data.to_csv(self.data_path + '//' + datafile_name,
                             sep=','
                             )