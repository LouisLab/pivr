import numpy as np
import matplotlib.pyplot as plt

STIM_TEST = False
PLOT_FREQ_VS_MINTIME = True

if STIM_TEST:
    '''
    Goal of this short script is to understand why I see flickering of the LED when quickly (every 30ms) changing PWM
    dutycycle.
    '''

    import pigpio
    import numpy as np
    import time

    pi = pigpio.pi()

    # this is here to normalize
    pi.set_PWM_range(17,40000)
    pi.set_PWM_dutycycle(17,50)

    pi.set_PWM_range(27, 40000)

    stimulus = np.zeros((1000))
    stimulus[::2] = 1

    stimulus[:] += 25

    stimulus[0,1] = time.time()
    for i in range(stimulus.shape[0]):
        pi.set_PWM_dutycycle(27, stimulus[i])

        time.sleep(0.03)
        print(stimulus[i])

if PLOT_FREQ_VS_MINTIME:
    '''
    The following script should make a plot that shows us the relationship between the minimal time that can be used
    by the Raspberry GPIO - my measurments indicate (maybe repeat with a proper osciloscope)that the minimal time needed
    for ON (or probably OFF) is 10us. This means that if I have a very high frequency, lets say 1000Hz (one cycle = 1ms)
    and a dutycycle of 1% I'll have a pulse width of exactly 10us - the measured limit the Raspberry can give me. 
    If one would now try to get a dutycycle of 0.5% it will fail to deliver the pulse (light more dimmed than expected)
    sometimes.
    This plot should help us configure a good compromise between frequency and minimal percentage that can be delivered.
    '''

    import matplotlib.pyplot as plt
    from matplotlib import cm

    min_time = 1e-05 # 10us
    frequencies = [8000,4000,1000,750,500,250,100,75,50,25,10]

    cm_subsection = np.linspace(0, 1, len(frequencies))
    colors = [cm.brg(x) for x in cm_subsection]

    def minimal_percentage_possible(frequency, minimal_time):
        time_per_tick = 1/frequency
        minimal_percentage = minimal_time/(time_per_tick)
        return(minimal_percentage)

    for i in range(len(frequencies)):
        if i == 0:
            min_perc = [minimal_percentage_possible(frequencies[i],min_time)]
        else:
            min_perc.append(minimal_percentage_possible(frequencies[i],min_time))

    def time_on_off_at_min_perc_possible(frequency, minimal_percentage):
        time_per_tick = 1/frequency
        time_on_per_tick = time_per_tick * minimal_percentage
        time_off_per_tick = time_per_tick - time_on_per_tick
        #print('TickTime: ' + repr(time_per_tick) + ' time on: ' + repr(time_on_per_tick) + ' time off: '+
        #      repr(time_off_per_tick))

        return(time_on_per_tick, time_off_per_tick)

    for i in range(len(frequencies)):
        if i == 0:
            time_on = [time_on_off_at_min_perc_possible(frequencies[i],min_perc[i])[0]]
            time_off = [time_on_off_at_min_perc_possible(frequencies[i],min_perc[i])[1]]
        else:
            time_on.append(time_on_off_at_min_perc_possible(frequencies[i],min_perc[i])[0])
            time_off.append(time_on_off_at_min_perc_possible(frequencies[i],min_perc[i])[1])

    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i in range(len(frequencies)):
        ax.scatter(x=time_off[i]*1000,y=min_perc[i]*100, color = colors[i])
        ax.annotate(repr(frequencies[i]) + 'Hz', (time_off[i]*1000, min_perc[i]*100))
    ax.set_xlabel('Off Time at minimal possible intensity percentage [ms]')
    ax.set_ylabel('Minimal possible intensity Percentage [%]')
    plt.grid()
