"""
I need to somehow visualize the experiments I've made for a figure in the paper. This is the first stab.
"""

import os
from skimage.measure import regionprops, label
import numpy as np
import json
from glob import glob
import matplotlib.pyplot as plt

#path = 'G:\\My Drive\\PhD\\SmAl-VR\\180221-vrtest3\\RaspiThao\\vulcano\\21.02.2018_18-00-52_MS379xMS133\\'
#path = 'G:\\My Drive\\PhD\\SmAl-VR\\180115 - MS002xUAS-TNT\\Animals from 180110 Danielle RasPi\\IAA\\'
#path = 'G:\\My Drive\\PhD\\SmAl-VR\\180621 - Thao startle\\june212018  stronger light\\21.06.2018_17-02-24_CantonS\\'
path = 'G:\My Drive\\PhD\\smAl-VR\\180221-vrtest3\\combined\\no_stimulus\\'

os.chdir(path)
dataset_names = [p.replace('\\', '') for p in glob('*/')]

for j in range(len(dataset_names)):

    os.chdir(path + dataset_names[j])

    with open((path + dataset_names[j] + '\\experiment_settings.json'), 'r') as file:
        experiment_settings = json.load(file)
        pixel_per_mm = experiment_settings['Pixel per mm']
        recording_time = experiment_settings['Recording time']
        fps = experiment_settings['Framerate']

    thresh = np.load('sm_thresh.npy')
    filled_areas = np.zeros((thresh.shape[2]-1))

    for i in range(thresh.shape[2]-1):
        filled_areas[i] = regionprops(label(thresh[:,:,i]))[0].filled_area

    if j == 0:
        filled_area_mm = np.zeros((thresh.shape[2]-1,len(dataset_names)))
        fig = plt.figure()
        ax = fig.add_subplot(111)

    filled_area_mm[:,j] = filled_areas/pixel_per_mm
    ax.plot(filled_area_mm[:,j], alpha = 0.75)#label = dataset_names[j],

ax.legend()