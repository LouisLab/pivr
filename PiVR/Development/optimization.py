import numpy as np
import os
import matplotlib.pyplot as plt
from glob import glob

pixel_per_mm = 5.12
path = 'Z:\\Tadres\\PhD\\SmAl-VR\\180119\\Danielle\\optimization_Data\\'
os.chdir(path)

all_files = [p.replace('\\', '') for p in glob('*')]


for i in range(len(all_files)):
    if i == 0:
        temp_data = np.load(all_files[i])
        data = np.zeros((temp_data.shape[0],temp_data.shape[1], len(all_files)))
        data[:,:,i] = temp_data
    else:
        try:
            data[0:data.shape[0],0:data.shape[1],i] = np.load(all_files[i])[0:data.shape[0], 0:data.shape[1]]
        except ValueError:
            other_temp = np.load(all_files[i])
            data[0:other_temp.shape[0], 0:other_temp.shape[1], i] = other_temp

data[:,0:] /= pixel_per_mm

freq_filled_area, edge_filled_area = np.histogram(data[:,0,:].ravel(), bins=30)
freq_eccentricity, edge_eccentricity = np.histogram(data[:,1,:].ravel(),bins=30)
freq_axis_ratio, edge_axis_ratio = np.histogram(data[:,2,:].ravel(), bins='auto')

fig = plt.figure()
ax_1 = fig.add_subplot(221)
ax_1.bar(edge_filled_area[:-1], freq_filled_area, width=np.diff(edge_filled_area), align="edge")
ax_1.set_title('Filled Area [pixel/(pixel/mm)]')

ax_2 = fig.add_subplot(222)
ax_2.bar(edge_eccentricity[:-1], freq_eccentricity, width=np.diff(edge_eccentricity), align="edge")
ax_2.set_title('Eccentricity with zero being a circle')

ax_3 = fig.add_subplot(212)
ax_3.bar(edge_axis_ratio[:-1], freq_axis_ratio, width=np.diff(edge_axis_ratio), align="edge")
ax_3.set_title('Major axis divided by minor axis')

#fig.suptitle('')
fig.tight_layout()
fig.savefig('180119 - Danielle.jpg')
