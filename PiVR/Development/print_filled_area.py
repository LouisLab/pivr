'''
A quick script to show me the filled area of a given animal

'''

import numpy as np
import os
import matplotlib.pyplot as plt
from skimage import measure

data_path = 'G:\My Drive\\PhD\\smAl-VR\\180221-vrtest3\\combined\\no_stimulus\\'
os.chdir(data_path)

binary_images = np.load('sm_thresh.npy')
filled_area = np.zeros((binary_images.shape[2]))
major_axis = np.zeros(binary_images.shape[2])

for i in range(binary_images.shape[2]):
    probs = measure.regionprops(measure.label(binary_images[:,:,i]))
    if len(probs) == 1:
        filled_area[i] = probs[0].filled_area
        major_axis[i] = probs[0].major_axis_length
